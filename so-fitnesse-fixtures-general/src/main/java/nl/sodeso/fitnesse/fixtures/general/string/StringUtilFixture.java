package nl.sodeso.fitnesse.fixtures.general.string;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;

import java.util.UUID;

/**
 * @author Ronald Mathies
 */
@Fixture(name="String Util Fixture")
public final class StringUtilFixture {

    @Start(name="Start String Fixture", arguments = {}, example = "|start |String Fixture|")
    public StringUtilFixture() {}

    @Command(name="Is Equal", arguments = {"value1", "value2"}, example = "|isEqual; |value1|value2|")
    public boolean isEqual(String value1, String value2) {
        if (value1 != null && value2 != null) {
            return value1.equals(value2);
        }

        if (value1 == null && value2 == null ) {
            return true;
        }

        return false;
    }

    @Command(name="Is Equal Ignore Case", arguments = {"value1", "value2"}, example = "|isEqualIgnoreCase; |value1|value2|")
    public boolean isEqualIgnoreCase(String value1, String value2) {
        if (value1 != null && value2 != null) {
            return value1.equalsIgnoreCase(value2);
        }

        if (value1 == null && value2 == null ) {
            return true;
        }

        return false;
    }

    @Command(name="Is Null", arguments = {"value"}, example = "|isNull; |value|")
    public boolean isNull(String value) {
        return value == null;
    }

    @Command(name="Is Not Null", arguments = {"value"}, example = "|isNotNull; |value|")
    public boolean isNotNull(String value) {
        return !isNull(value);
    }

    @Command(name="Is Not Empty", arguments = {"value"}, example = "|isNotEmpty; |value|")
    public boolean isNotEmpty(String value) {
        return value != null && !value.isEmpty();
    }

    @Command(name="Is Not Empty", arguments = {"value"}, example = "|isNotEmpty; |value|")
    public boolean isEmpty(String value) {
        return value != null && value.isEmpty();
    }

    @Command(name="Random UUID", arguments = {}, example = "|$value= |randomUuid;|")
    public String randoomUuid() {
        UUID uuid = UUID.randomUUID();
        return Long.toHexString(uuid.getMostSignificantBits());
    }

}
