package nl.sodeso.fitnesse.fixtures.general.number;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Number Util Fixture")
public class NumberUtilFixture {

    /**
     * Constructs a new Number Util Fixture.
     */
    @Start(name="Start Number Util Fixture", arguments = {}, example = "|start |Number Util Fixture|")
    public NumberUtilFixture() {}

    /**
     * Adds the given amount to the number.
     *
     * @param number the number.
     * @param amount the amount to add.
     *
     * @return the result.
     */
    @Command(name="Add", arguments = {"number", "amount"}, example = "|$today=|add; |10|5|")
    public int add(int number, int amount) {
        return number + amount;
    }

    /**
     * Subtracts the given amount to the number.
     *
     * @param number the number.
     * @param amount the amount to subtract.
     *
     * @return the result.
     */
    @Command(name="Subtract", arguments = {"number", "amount"}, example = "|$today=|subtract; |10|5|")
    public int subtract(int number, int amount) {
        return number - amount;
    }

}
