package nl.sodeso.fitnesse.fixtures.general.string;


import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.fitnesse.CommandException;

/**
 * Fixture for loading a properties file.
 *
 * @author Ronald Mathies
 */
@Fixture(name="String Util Fixture")
public class StringFixture {

    private StringBuilder builder = new StringBuilder();

    /**
     * Constructs a new String Fixture with the initial value set.
     *
     * @param value the initial value to work with.
     */
    @Start(name="Start String Util Fixture", arguments = {"value"}, example = "|start |String Fixture|value|")
    public StringFixture(String value) {
        builder.append(value);
    }

    /**
     * Clears the buffer completely.
     * @param value the value to append.
     * @return true when the command was successful.
     */
    @Command(name="Append Value", arguments = {"value"}, example = "|appendValue; |value|")
    public boolean appendValue(String value) {
        if (value != null && !value.isEmpty()) {
            builder.append(value);
        }

        return true;
    }

    /**
     * Clears the buffer completely.
     * @return true when the command was successful.
     */
    @Command(name="Clear Buffer", arguments = {}, example = "|clearBuffer; |")
    public boolean clearBuffer() {
        builder = new StringBuilder();
        return true;
    }

    /**
     * Returns the value.
     * @return the value.
     */
    @Command(name="Result", arguments = {}, example = "|$value= |result;|")
    public String result() {
        return builder.toString();
    }

    /**
     * Replaces the pattern with the replacement in the buffer.
     *
     * @param pattern the pattern to look for.
     * @param replacement the replacement value.
     * @return true when the command was successful.
     */
    @Command(name="Replace All", arguments = {"pattern", "replacement"}, example = "|replaceAll; |pattern|replacement|")
    public boolean replaceAll(String pattern, String replacement) {
        String value = builder.toString();
        builder = new StringBuilder(value.replaceAll(pattern, replacement));
        return true;
    }

    /**
     * eturns a new string that is a substring of this string.
     *
     * @param start the start index.
     * @param length the end index.
     * @return the part of the string.
     */
    @Command(name="Substring", arguments = {"start", "length"}, example = "|$value= |substring;|start|length|")
    public String substring(int start, int length) {
        String value = builder.toString();

        if (start < 0) {
            throw new CommandException("De start '%s' can not be a negative value.", start);
        }
        if (length < 0) {
            throw new CommandException("The length '%d' cannot be a negative value", length);
        }
        if (start + length > value.length()) {
            throw new CommandException("The start '%d' plus the length '%d' cannot be larger then the length '%d' of the value.", start, length, value.length());
        }


        return value.substring(start, start + length);
    }

}
