package nl.sodeso.fitnesse.fixtures.general.bsn;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.fitnesse.ConditionalExceptionType;

import java.util.Random;

import static nl.sodeso.fitnesse.fixtures.util.FixtureOperations.equal;

/**
 * @author Ronald Mathies
 */
@Fixture(name="BSN Generator Fixture")
public class BsnGenerator {

    private static final int LOW = 100000000;
    private static final int HIGH = 999999988;

    /**
     * Constructs a new BSN Generator.
     */
    @Start(name="startBsnGenerator", arguments = {}, example = "|start |Bsn Generator|")
    public BsnGenerator() {
    }

    /**
     * Returns a random BSN number.
     *
     * In the range 100000000 - 999999988
     */
    @Command(name="generateBsn", arguments = {}, example = "|$bsn= |generateBsn;|")
    public String generateBsn() {
        int possibleBsn = giveRandomStartingNumber();

        for (;;++possibleBsn) {
            if (checkElfProef(possibleBsn)) {
                return String.valueOf(possibleBsn);
            }

            if (possibleBsn > HIGH) {
                ConditionalExceptionType.fail("Geen BSN gevonden.");
            }
        }
    }

    @Command(name="generateBsnInRange", arguments = {"low", "high"}, example = "|$bsn= |generateBsn;|low - ex. 100000000|high - ex. 999999988|")
    public String generateBsnInRange(int begin, int end) {
        int possibleBsn = giveRandomStartingNumber(begin, end);

        for (;;++possibleBsn) {
            if (checkElfProef(possibleBsn)) {
                return String.valueOf(possibleBsn);
            }

            if (possibleBsn > end) {
                ConditionalExceptionType.fail("Geen BSN gevonden.");
            }
        }
    }




    /**
     * Usefull in testdata generation.
     * In some cases a generated bsn is not usable. A check on the generated bsn can be performed end the results can be passed to this method.
     * If the passed values are equal a new bsn will be generated, otherwise the original bsn will be returned
     * @param originalBsn
     * @param valueOne
     * @param valueTwo
     * @return original bsn or a newly generated bsn
     */
    @Command(name = "generateBsnAgainIfConditionIsEqual", arguments = {"originalBsn", "valueOne", "valueTwo"}, example = "|generateBsnAgainIfConditionIsEqual|originaBsn|valueOne to compare|valueTwo to compare|")
    public String generateBsnAgainIfConditionIsEqual(String originalBsn, String valueOne, String valueTwo) {
        if (equal(valueOne, valueTwo)){
            return generateBsn();
        }
        else return originalBsn;
    }

    @Command(name = "generateBsnInRangeAgainIfConditionIsEqual", arguments = {"originalBsn", "low", "high", "valueOne", "valueTwo"}, example = "|generateBsnInRangeAgainIfConditionIsEqual|originaBsn|low - ex. 100000000|high - ex. 999999988|valueOne to compare|valueTwo to compare|")
    public String generateBsnInRangeAgainIfConditionIsEqual(String originalBsn,int begin, int end, String valueOne, String valueTwo) {
        if (equal(valueOne, valueTwo)){
            return generateBsnInRange(begin, end);
        }
        else return originalBsn;
    }


    private int giveRandomStartingNumber() {
        return new Random().nextInt(HIGH - LOW) + LOW;
    }

    private int giveRandomStartingNumber(int begin, int end) {
        return new Random().nextInt(end - begin) + begin;
    }

    /**
     * Checks the supplied number using the '11-proef voor een bsn'
     *      https://nl.wikipedia.org/wiki/Burgerservicenummer
     *      Als het burgerservicenummer wordt voorgesteld door ABCDEFGHI, dan moet:
     *      (9 × A) + (8 × B) + (7 × C) + (6 × D) + (5 × E) + (4 × F) + (3 × G) + (2 × H) + (-1 × I) moet een veelvoud van 11 zijn.
     * @param bsn
     * @return true or false. If the number supplied does not have a length of 9 characters an exception.
     */

    @Command(name="checkElfProef", arguments = {"bsn"}, example = "|checkElfProef;|bsn|")
    public  boolean checkElfProef(int bsn) {
        if (String.valueOf(bsn).length() == 9) {
            int A, B, C, D, E, F, G, H, I, pos = 0;

            A = giveNumberOnPosition(bsn, pos++);
            B = giveNumberOnPosition(bsn, pos++);
            C = giveNumberOnPosition(bsn, pos++);
            D = giveNumberOnPosition(bsn, pos++);
            E = giveNumberOnPosition(bsn, pos++);
            F = giveNumberOnPosition(bsn, pos++);
            G = giveNumberOnPosition(bsn, pos++);
            H = giveNumberOnPosition(bsn, pos++);
            I = giveNumberOnPosition(bsn, pos);

            int resultaat = (9 * A) + (8 * B) + (7 * C) + (6 * D) + (5 * E) + (4 * F) + (3 * G) + (2 * H) + (-1 * I);

            return resultaat % 11 == 0;
        } else {
            ConditionalExceptionType.fail("Het opgegeven nummer bevat geen 9 karakters");
        }
        return false; //never used
    }
    
    private int giveNumberOnPosition(int bsn, int position) {
        String number = String.valueOf(bsn);
        String substring = number.substring(position, position + 1);
        return Integer.parseInt(substring);
    }

}
