package nl.sodeso.fitnesse.fixtures.general.file;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.commons.fileutils.FileFinder;
import nl.sodeso.fitnesse.CommandException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Fixture for manipulating a file.
 *
 * @author Ronald Mathies
 */
@Fixture(name="File Fixture")
public class FileFixture {

    private String filename = null;
    private File file = null;

    /**
     * Finds the file with the given filename.
     *
     * @param filename the name of the file.
     */
    @Start(name="Start File Fixture", arguments = {"file"}, example = "|start |File Fixture|file.txt|")
    public FileFixture(String filename) {
        this.filename = filename;

        FileFinder.findFileInUserPath(filename, found -> {
            file  = found;
        });

        if (file == null) {
            throw new CommandException("FileFixture cannot find file '%s', please make sure the file exists and is on the classpath.", filename);
        }
    }

    /**
     * Loads the contents of the found file and returns it as plain text.
     *
     * @return the contents of the file as plain text.
     */
    @Command(name="Load As Text", arguments = {}, example = "|$value= |loadAsText; |")
    public String loadAsText() {
        try {
            return new String(Files.readAllBytes(this.file.toPath()), "UTF-8");
        } catch (IOException e) {
            throw new CommandException("FileFixture cannot load file '%s'.", filename);
        }
    }

}
