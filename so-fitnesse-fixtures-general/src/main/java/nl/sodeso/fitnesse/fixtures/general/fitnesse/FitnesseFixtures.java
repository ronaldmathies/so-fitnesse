package nl.sodeso.fitnesse.fixtures.general.fitnesse;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import nl.sodeso.fitnesse.CommandException;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Fitnesse")
public class FitnesseFixtures {

    public FitnesseFixtures() {
        throw new CommandException("Do not use this fixture directly, it functions as a template only.");
    }

    @Command(name="Push Fixture", arguments = {}, example = "|push fixture|")
    public boolean pushFixture() {
        return false;
    }

    @Command(name="Pop Fixture", arguments = {}, example = "|pop fixture|")
    public boolean popFixture() {
        return false;
    }

    @Command(name="Import Fixture", arguments = {}, example = "|import|\n|nl.sodeso.fixture|")
    public boolean importFixture() {
        return false;
    }

    @Command(name="Add Collapsible Table (default collapsed)", arguments = {}, example = "!***> Title Of Block\n!|script|\n|command;|\n*!")
     public boolean addCollapableTableType1() {
        return false;
    }

    @Command(name="Add Collapsible Table", arguments = {}, example = "!*** Title Of Block\n!|script|\n|command;|\n*!")
    public boolean addCodeBlock() {
        return false;
    }

    @Command(name="Add code Block", arguments = {}, example = "{{{\ncode\nblock\n}}}")
    public boolean createCollapableTableType2() {
        return false;
    }

    @Command(name="Add Documentation Block", arguments = {},
            example =
                    "DOCUMENTATION-BEGIN\n" +
                    "DOCUMENTATION-TESTCASE TestSuite.StartApplication*\n" +
                    "DOCUMENTATION-LINK http://www.sodeso.nl\n" +
                    "DOCUMENTATION-PURPOSE Short description\n" +
                    "Long description ...\n" +
                    "DOCUMENTATION-END\n")
    public boolean addDocumentationBlock() { return false; }
}
