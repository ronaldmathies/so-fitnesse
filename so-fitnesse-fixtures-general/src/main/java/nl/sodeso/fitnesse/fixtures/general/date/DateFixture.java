package nl.sodeso.fitnesse.fixtures.general.date;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.fitnesse.CommandException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Date Fixture")
public final class DateFixture {

    private Calendar calendar = Calendar.getInstance();

    /**
     * Constructs a new Date Util Fixture.
     */
    @Start(name="Start Date Fixture", arguments = {}, example = "|start |Date Fixture|")
    public DateFixture() {
        calendar = Calendar.getInstance();
    }

    /**
     * Initializes the calendar with a date based on the user input.
     *
     * @param format the date format.
     * @param date the date.
     */
    @Command(name="Init With String Date", arguments = {"format", "date"}, example = "|initWithStringDate; |dd-MM-yyyy|01-01-2015|")
    public void initWithStringDate(String format, String date) {
        calendar = Calendar.getInstance();
        calendar.setTime(stringToDate(format, date));
    }

    /**
     * Returns the date as a string with the given format.
     *
     * @param format the format.
     * @return the date as a string.
     */
    @Command(name="Result As String", arguments = {"format"}, example = "|$value= |resultAsString|dd-MM-yyyy|")
    public String resultAsString(String format) {
        return DateUtilFixture.dateToString(calendar.getTime(), format);
    }

    /**
     * Adds a number of days to the date.
     *
     * @param days the number of days to add.
     * @return true if the days have been added.
     */
    @Command(name="Add Days", arguments = {"days"}, example = "|addDays; |5|")
    public boolean addDays(int days) {
        calendar.roll(Calendar.DAY_OF_YEAR, days);
        return true;
    }

    /**
     * Subtracts a number of days from the date.
     *
     * @param days the number of days to subtract.
     * @return true if the days have been subtracted.
     */
    @Command(name="Subtract Days", arguments = {"days"}, example = "|addDays; |5|")
    public boolean subtractDays(int days) {
        calendar.roll(Calendar.DAY_OF_YEAR, days * -1);
        return true;
    }

    /**
     * Adds a number of months to the date.
     *
     * @param months the number of months to add.
     * @return true if the months have been added.
     */
    @Command(name="Add Months", arguments = {"months"}, example = "|addMonths; |5|")
    public boolean addMonths(int months) {
        calendar.roll(Calendar.MONTH, months);
        return true;
    }

    /**
     * Subtracts a number of months from the date.
     *
     * @param months the number of months to subtract.
     * @return true if the months have been subtracted.
     */
    @Command(name="Subtract Months", arguments = {"months"}, example = "|subtractMonths; |5|")
    public boolean subtractMonths(int months) {
        calendar.roll(Calendar.MONTH, months * -1);
        return true;
    }

    /**
     * Adds a number of years to the date.
     *
     * @param years the number of years to add.
     * @return true if the years have been added.
     */
    @Command(name="Add Years", arguments = {"years"}, example = "|addYears; |5|")
    public boolean addYears(int years) {
        calendar.roll(Calendar.YEAR, years * 1);
        return true;
    }

    /**
     * Subtracts a number of years from the date.
     *
     * @param years the number of years to subtract.
     * @return true if the years have been subtracted.
     */
    @Command(name="Subtract Years", arguments = {"years"}, example = "|subtractYears; |5|")
    public boolean subtractYears(int years) {
        calendar.roll(Calendar.YEAR, years * -1);
        return true;
    }

    private Date stringToDate(String format, String date) {
        try {
            return new SimpleDateFormat(format).parse(date);
        } catch (ParseException parseException) {
            throw new CommandException("Failed to parse the date '%s' with the specified format '%s'", date, format);
        }
    }
}
