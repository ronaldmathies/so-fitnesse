package nl.sodeso.fitnesse.fixtures.general.webdriver;

import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.commons.fileutils.FileFinder;
import nl.sodeso.commons.fileutils.FileHandler;
import nl.sodeso.fitnesse.CommandException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Internet Explorer Fixture")
public class InternetExplorerFixture {

    private Properties properties = null;
    private File internetExplorerDriver = null;

    /**
     * Initializes the extra Internet Explorer driver based on settings in the given properties file.
     * @param file the properties file continaing the settings for Internet Explorer
     */
    @Start(name="Start Internet Explorer Fixture", arguments = {"file"}, example = "|start |Internet Explorer Fixture|internetexplorer.properties|")
    public InternetExplorerFixture(String file) {

        FileFinder.findFileInUserPath(file, new FileHandler() {
            @Override
            public void handle(File file) {
                try {
                    properties = new Properties();
                    properties.load(new FileReader(file));
                } catch (IOException e) {
                    throw new IllegalArgumentException("InternetExplorerFixture cannot load file '" + file + "', please make sure the file exists and is on the classpath.");
                }
            }
        });

        if (properties == null) {
            throw new IllegalArgumentException("InternetExplorerFixture cannot load file '" + file + "', please make sure the file exists and is on the classpath.");
        }

        if (!properties.containsKey("webdriver.ie.driver")) {
            throw new CommandException("InternetExplorerFixture has trouble detecting the driver it need's to use, 'webdriver.ie.driver' is missing in the '%s' configuration file.", file);
        }

        String internetExplorerDriverVersion = (String)properties.get("webdriver.ie.driver");
        FileFinder.findFileInUserPath("IEDriverServer" + internetExplorerDriverVersion + ".exe", new FileHandler() {
            @Override
            public void handle(File file) {
                internetExplorerDriver = file;
            }
        });

        if (internetExplorerDriver == null) {
            throw new CommandException("InternetExplorerFixture has trouble detecting the driver it need's to use, '%s' is not a valid option.", internetExplorerDriverVersion);
        }

        System.setProperty("webdriver.ie.driver", internetExplorerDriver.getPath());
    }

}