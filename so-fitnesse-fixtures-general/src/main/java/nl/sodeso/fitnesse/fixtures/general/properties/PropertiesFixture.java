package nl.sodeso.fitnesse.fixtures.general.properties;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.commons.fileutils.FileFinder;
import nl.sodeso.commons.fileutils.FileHandler;
import nl.sodeso.fitnesse.CommandException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Fixture for loading a properties file.
 *
 * @author Ronald Mathies
 */
@Fixture(name="Properties Fixture")
public class PropertiesFixture {

    private Properties properties = null;

    /**
     * Load the given properties file, the file can be anywhere on the user.dir
     * path. It will use the FileFinder to recursively search the path for the given file.
     *
     * @param file the properties file.
     */
    @Start(name="Start Properties Fixture", arguments = {"file"}, example = "|start |Properties Fixture|file.properties|")
    public PropertiesFixture(String file) {
        FileFinder.findFileInUserPath(file, new FileHandler() {
            @Override
            public void handle(File file) {
                try {
                    properties = new Properties();
                    properties.load(new FileReader(file));
                } catch (IOException e) {
                    throw new IllegalArgumentException("PropertiesFixture cannot load file '" + file + "', please make sure the file exists and is on the classpath.");
                }
            }
        });

        if (properties == null) {
            throw new IllegalArgumentException("PropertiesFixture cannot load file '" + file + "', please make sure the file exists and is on the classpath.");
        }
    }

    /**
     * Returns the string value of the given property key.
     *
     * @param key the key to search for.
     *
     * @return the string value.
     *
     * @throws CommandException when the property key cannot be found.
     */
    @Command(name="Get String Property", arguments = {"key"}, example = "|$value= |getStringProperty; |key|")
    public String getStringProperty(String key) {
        if (properties.containsKey(key)) {
            return properties.getProperty(key);
        }

        throw new CommandException("PropertiesFixture cannot get property with key '%s', please make sure the property with that key exists.", key);
    }

    /**
     * Returns the int value of the given property key.
     *
     * @param key the key to search for.
     *
     * @return the int value.
     *
     * @throws CommandException when the property key cannot be found or the property
     * key doesn't contain a integer value.
     */
    @Command(name="Get Int Property", arguments = {"key"}, example = "|$value= |getIntProperty; |key|")
    public int getIntProperty(String key) {
        try {
            return Integer.parseInt(getStringProperty(key));
        } catch (NumberFormatException e) {
            throw new CommandException("PropertiesFixture cannot get int property value with key '%s', please make sure the property contains an integer value.", key);
        }
    }

}
