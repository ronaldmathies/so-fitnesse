package nl.sodeso.fitnesse.fixtures.general.pdf;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.fitnesse.ConditionalExceptionType;
import nl.sodeso.fitnesse.fixtures.general.util.FileUtils;
import nl.sodeso.fitnesse.fixtures.general.util.PDFUtils;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Base64;

/**
 * Ik ben een fixture voor integratie en systeemtesten, bedoelt voor
 * aanroep van uit Fitnesse. Ik kan een PDF bestand inlezen en daar
 * tekst uit filteren.
 *
 * @author dbxman
 */
@Fixture(name = "PDF Text Reader Fixture")
public class PdfTextReaderFixture {

	private PDDocument pdf = null;

	@Start(name = "startPdfTextReaderFixture", arguments = {}, example = "|start |Pdf Text Reader Fixture|")
	public PdfTextReaderFixture() {
	}

	@Command(name = "loadPdfFromFile", arguments = {"file"}, example = "|loadPdfFromFile; |file|")
	public boolean loadPdfFromFile(String file) {
		try {
			pdf = PDFUtils.loadPDF(FileUtils.file(file));
			return pdf != null;

		} catch (IOException e) {
			ConditionalExceptionType.fail("Error loading PDF document '%s', the following exception occurred '%s'", file, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "loadPdfFromUrl", arguments = {"url"}, example = "|loadPdfFromUrl; |url|")
	public boolean loadPdfFromUrl(String url) {
		try {
			pdf = PDFUtils.loadPdf(new URL(url));
			return pdf != null;
		} catch (IOException e) {
			ConditionalExceptionType.fail("Error loading PDF document '%s', the following exception occurred '%s'", url, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "loadPdfFromUrl", arguments = {"url", "username", "password"}, example = "|loadPdfFromUrl; |url|username|password|")
	public boolean loadPdfFromUrl(String url, final String username, final String password) {
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return (new PasswordAuthentication(username, password.toCharArray()));
			}
		};
		Authenticator.setDefault(auth);
		return loadPdfFromUrl(url);
	}

	@Command(name = "loadPdfFromStream", arguments = {"inputstream"}, example = "|loadPdfFromStream; |inputstream|")
	public boolean loadPdfFromStream(InputStream is) {
		try {
			pdf = PDFUtils.loadPdfFromStream(is);
			return pdf != null;

		} catch (IOException e) {
			ConditionalExceptionType.fail("Error loading PDF document from inputstream, the following exception occurred '%s'", e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "loadPdfFromBase64String", arguments = {"base64Encoded string"}, example = "|loadPdfFromBase64String;|$variableContainingTheBase64EncodedPdfFile|")
	public boolean loadPdfFromBase64String(String base64Encoded) {
		byte[] decoded = Base64.getDecoder().decode(base64Encoded);
		InputStream stream = new ByteArrayInputStream(decoded);
		loadPdfFromStream(stream);
		return pdf != null;
	}


//    @Command(name="loadPdfFromDatabase", arguments = {"jdbcurl", "username", "password", "sql"}, example = "|loadPdfFromDatabase; |jdbcurl|username|password|sql|")
//    public boolean loadPdfFromDatabase(String jdbcurl, String username, String password, String sql) {
//        try {
//            pdf = PDFUtils.laadPdfVanStream(new SimpleSqlQueryFixture(jdbcurl, username, password).getBlobAsSteam(sql));
//            return pdf != null;
//        } catch (IOException e) {
//            ConditionalExceptionType.fail("Error loading PDF document '%s', '%s', the following exception occurred '%s'", jdbcurl, sql, e.getMessage());
//        } catch (SQLException e) {
//            ConditionalExceptionType.fail("Error loading PDF document '%s', '%s', the following exception occurred '%s'", jdbcurl, sql, e.getMessage());
//        } catch (ClassNotFoundException e) {
//            ConditionalExceptionType.fail("Error loading PDF document '%s', '%s', the following exception occurred '%s'", jdbcurl, sql, e.getMessage());
//        }
//    }

	@Command(name = "closePdf", arguments = {}, example = "|closePdf; |")
	public void closePdf() {
		try {
			PDFUtils.closePDF(pdf);

		} catch (IOException e) {
			ConditionalExceptionType.fail("Error closing PDF document, the following exception occurred '%s'", e.getMessage());
		}
	}

	@Command(name = "pdfToString", arguments = {"sortByPosition"}, example = "|$pdfText= |pdfToString; |sortByPosition|")
	public String pdfToString(boolean sortByPosition) {
		try {
			return PDFUtils.pdfText(pdf, sortByPosition);

		} catch (IOException e) {
			ConditionalExceptionType.fail("Error converting PDF to plain text '%s'", e.getMessage());
		}
		return null; //never used
	}

	@Command(name = "pdfToTextLines", arguments = {}, example = "|$pdfTextLines= |pdfToTextLines; |")
	public String[] pdfToTextLines() {
		try {
			return PDFUtils.pdfTextLines(pdf);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error converting PDF to plain text lines '%s'", e.getMessage());
		}
		return null; //never used
	}

	@Command(name = "pdfContainsTextLine", arguments = {"text"}, example = "|pdfContainsTextLine; |text|")
	public boolean pdfContainsTextLine(String text) {
		try {
			return PDFUtils.pdfContainsLine(pdf, text);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of the following line of text '%s', the following exception occurred '%s'", text, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "pdfContainsTextLineCaseInsensitive", arguments = {"text"}, example = "|pdfContainsTextLineCaseInsensitive; |text|")
	public boolean pdfContainsTextLineCaseInsensitive(String text) {
		try {
			return PDFUtils.pdfContainsLineCaseInsensitive(pdf, text);

		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of the following line (case insensitive) of text '%s', the following exception occurred '%s'", text, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "pdfContainsTextLineWithPrefix", arguments = {"prefix"}, example = "|pdfContainsTextLineWithPrefix; |prefix|")
	public boolean pdfContainsTextLineWithPrefix(String prefix) {
		try {
			return PDFUtils.pdfLineStartsWith(pdf, prefix);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of a line with the prefix '%s', the following exception occurred '%s'", prefix, e.getMessage());
		}
		return false; //never used
	}

	/**
	 * Controleert of een regel met de aangeleverde eindigt
	 */
	@Command(name = "pdfContainsTextLineWithSuffix", arguments = {"suffix"}, example = "|pdfContainsTextLineWithSuffix; |suffix|")
	public boolean pdfContainsTextLineWithSuffix(String suffix) {
		try {
			return PDFUtils.pdfLineEndsOn(pdf, suffix);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of a line with the suffix '%s', the following exception occurred '%s'", suffix, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "pdfContainsTextLineThatContainsText", arguments = {"text"}, example = "|pdfContainsTextLineThatContainsText; |text|")
	public boolean pdfContainsLineThatContainsText(String text) {
		try {
			return PDFUtils.pdfLineContains(pdf, text);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of a line that contains the text '%s', the following exception occurred '%s'", text, e.getMessage());
		}
		return false; //never used
	}


	@Command(name = "pdfContainsTextBetweenOnPage", arguments = {"page", "fromText", "toText", "expectedText"}, example = "|pdfContainsTextBetweenOnPage; |page|fromText|toText|expectedText|")
	public boolean pdfContainsTextBetweenOnPage(int page, String fromText, String toText, String expectedText) {
		try {
			return PDFUtils.pdfContainsTextBetweenTwoTextsOnOnePage(pdf, page, fromText, toText, expectedText);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of the text '%s' on page '%s' between '%s' and '%s', the following exception occurred '%s'", expectedText, page, fromText, toText, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "pdfContainsTextBetweenOnPageInsideBlock", arguments = {"page", "fromText", "fromTextBlockNr", "toText", "expectedText"}, example = "|pdfContainsTextBetweenOnPageInsideBlock; |page|fromText|fromTextBlockNr|toText|expectedText|")
	public boolean pdfContainsTextBetweenOnPageInsideBlock(int page, String fromText, int fromTextBlockNr, String toText, String expectedText) {
		try {
			return PDFUtils.pdfContainsTextBetweenTwoTextsInOneBlockOnOnePage(pdf, page, fromText, fromTextBlockNr, toText, expectedText);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of the text '%s' on page '%s' between '%s' from block '%s' and '%s', the following exception occurred '%s'", expectedText, page, fromText, fromTextBlockNr, toText, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "pdfContainsTextBetweenOnPage", arguments = {"page", "fromText", "toText", "unexpectedText"}, example = "|pdfContainsTextBetweenOnPage; |page|fromText|toText|unexpectedText|")
	public boolean pdfNotContainsTextBetweenOnPage(int page, String fromText, String toText, String unexpectedText) {
		try {
			return !PDFUtils.pdfContainsTextBetweenTwoTextsOnOnePage(pdf, page, fromText, toText, unexpectedText);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the absence of the text '%s' on page '%s' between '%s' and '%s', the following exception occurred '%s'", unexpectedText, page, fromText, toText, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "pdfContainsTextOnPage", arguments = {"page", "expectedText"}, example = "|pdfContainsTextOnPage; |page|expectedText|")
	public boolean pdfContainsTextOnPage(int page, String expectedText) {
		try {
			return PDFUtils.pdfTextPresentOnAPage(pdf, page, expectedText);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the existence of the text '%s' on page '%s', the following exception occurred '%s'", expectedText, page, e.getMessage());
		}
		return false; //never used
	}

	@Command(name = "pdfCountTextPresence", arguments = {"expectedText"}, example = "|pdfCountTextPresence; |expectedText|")
	public int pdfCountTextPresence(String expectedText) {
		try {
			return PDFUtils.countTimesPresent(pdf, expectedText);
		} catch (Exception e) {
			ConditionalExceptionType.fail("Error checking PDF for the number of times that the text '%s' is in the document, the following exception occurred '%s'", expectedText, e.getMessage());
		}
		return 0; //never used
	}

	@Command(name = "writePdfToFile", arguments = {"filename"}, example = "|writePdfToFile; |filename|")
	public void writePdfToFile(String filename) {
		try {

			File file = new File(filename);
			if (!file.exists()) {
				FileUtils.createFile(file);
			}
			this.pdf.save(filename);

		} catch (IOException e) {
			ConditionalExceptionType.fail("Error writing PDF to file '%s', the following exception occurred '%s'", e.getMessage());

		} catch (COSVisitorException e) {
			ConditionalExceptionType.fail("Error writing PDF to file '%s', the following exception occurred '%s'", e.getMessage());
		}
	}
}

