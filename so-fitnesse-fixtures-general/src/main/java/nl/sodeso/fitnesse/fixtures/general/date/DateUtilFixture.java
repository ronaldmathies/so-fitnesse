package nl.sodeso.fitnesse.fixtures.general.date;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Date Util Fixture")
public final class DateUtilFixture {

    private static String defaultDateFormat = "dd-MM-yyyy";

    /**
     * Constructs a new Date Util Fixture.
     */
    @Start(name="Start Date Util Fixture", arguments = {}, example = "|start |Date Util Fixture|")
    public DateUtilFixture() {
    }

    /**
     * Returns todays date with the default format.
     *
     * @return string formatted date.
     */
    @Command(name="Today", arguments = {}, example = "|$today=|today; |")
    public String today() {
        return today(defaultDateFormat);
    }

    /**
     * Returns todays date with the specified date format.
     *
     * @param format the date format.
     *
     * @return string formatted date.
     */
    @Command(name="Today", arguments = {"format"}, example = "|$today=|today; |dd-MM-yyyy|")
    public String today(String format) {
        return dateToString(Calendar.getInstance().getTime(), format);
    }

    /**
     * Returns todays date minus a number of days in default format.
     *
     * @param days the number of days in the past.
     *
     * @return string formatted date.
     */
    @Command(name="Today Minus Days", arguments = {"days"}, example = "|$date=|todayMinusDays; |10|")
    public String todayMinusDays(int days) {
        return todayMinusDays(defaultDateFormat, days);
    }

    /**
     * Returns todays date minus a number of days in the specified format.
     *
     * @param format the date format.
     * @param days the number of days in the past.
     *
     * @return string formatted date.
     */
    @Command(name="Today Minus Days", arguments = {"format", "days"}, example = "|$date=|todayMinusDays; |dd-MM-yyyy|10|")
    public String todayMinusDays(String format, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.DAY_OF_YEAR, days * -1);

        return dateToString(calendar.getTime(), format);
    }

    /**
     * Returns todays date plus a number of days in default format.
     *
     * @param days the number of days in the future.
     *
     * @return string formatted date.
     */
    @Command(name="Today Plus Days", arguments = {"format", "days"}, example = "|$date=|todayPlusDays; |10|")
    public String todayPlusDays(int days) {
        return todayPlusDays(defaultDateFormat, days);
    }

    /**
     * Returns todays date plus a number of days in the specified format.
     *
     * @param format the date format.
     * @param days the number of days in the future.
     *
     * @return string formatted date.
     */
    @Command(name="Today Plus Days", arguments = {"format", "days"}, example = "|$date=|todayPlusDays; |dd-MM-yyyy|10|")
    public String todayPlusDays(String format, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.DAY_OF_YEAR, days);

        return dateToString(calendar.getTime(), format);
    }

    /**
     * Returns todays date plus a number of years in default format.
     *
     * @param years the number of years in the future.
     *
     * @return string formatted date.
     */
    @Command(name="Today Plus Years", arguments = {"years"}, example = "|$date=|todayPlusYears; |10|")
    public String todayPlusYears(int years) {
        return todayPlusYears(defaultDateFormat, years);
    }

    /**
     * Returns todays date plus a number of years in the specified format.
     *
     * @param format the date format.
     * @param years the number of years in the future.
     *
     * @return string formatted date.
     */
    @Command(name="Today Plus Years", arguments = {"format", "years"}, example = "|$date=|todayPlusYears; |dd-MM-yyyy|10|")
    public String todayPlusYears(String format, int years) {
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.YEAR, years);

        return dateToString(calendar.getTime(), format);
    }

    /**
     * Returns todays date minus a number of years in the default format.
     *
     * @param years the number of years in the past.
     *
     * @return string formatted date.
     */
    @Command(name="Today Minus Years", arguments = {"years"}, example = "|$date=|todayMinusYears; |10|")
    public String todayMinusYears(int years) {
        return todayMinusYears(defaultDateFormat, years);
    }

    /**
     * Returns todays date minus a number of years in the specified format.
     *
     * @param format the date format.
     * @param years the number of years in the past.
     *
     * @return string formatted date.
     */
    @Command(name="Today Minus Years", arguments = {"format", "years"}, example = "|$date=|todayMinusYears; |dd-MM-yyyy|10|")
    public String todayMinusYears(String format, int years) {
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.YEAR, years * -1);

        return dateToString(calendar.getTime(), format);
    }

	/**
	 * Generates a unix timestamp in milliseconds from January 1, 1970 (midnight UTC/GMT), not counting leap seconds (in ISO 8601: 1970-01-01T00:00:00Z).
	 * More info on https://en.wikipedia.org/wiki/Unix_time
	 * This method uses the local systems timezone!
	 *
	 * @return timestamp in milliseconds since January 1, 1970 (midnight UTC/GMT) in the LOCAL timezone.
	 */
	@Command(name = "giveUnixTimestamp", arguments = {}, example = "|$localTimeZoneTimeStamp=|giveUnixTimestamp;|")
	public long giveUnixTimestamp() {
		Instant now = Instant.now();
		return now.toEpochMilli();
	}

	/**
	 * Converts a unix timestamp in milliseconds to the specified output format. Method assumes the local time zone is used.
	 * @param unixTime input unix timestamp (local timezone)
	 * @param dateFmt output dateTime format
	 * @return date (or dateTime) in the specified format
	 */
	@Command(name = "convertUnixTimeToFormat", arguments = {"unixTime", "output dateformat"}, example = "|$humanReadableTimeStamp=|convertUnixTimeToFormat;|1451602800000|dd-MM-yyyy HH:mm:ss.SSS")
	public String convertUnixTimeToFormat(long unixTime, String dateFmt) {
		String Iso8601Time = Instant.ofEpochMilli(unixTime).toString();
		DateTimeFormatter frmt = DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.systemDefault());
		LocalDateTime input = LocalDateTime.parse(Iso8601Time, frmt);
		return input.format(getFormat(dateFmt));
	}

	/**
	 * Converts an input date / date timestamp to unix time. Method assumes the local time zone is used.
	 * @param dateTime dateStamp or dateTimeStamp (local timezone)
	 * @param dateFmt input date format
	 * @return unix timeStamp
	 */
	@Command(name = "convertDateTimeToUnixTime", arguments = {"dateTimeStamp", "input format for the dateTimeStamp"}, example = "|$unixTime=|convertDateTimeToUnixTime|01-01-2016 00:00:00.000|dd-MM-yyyy HH:mm:ss.SSS|")
	public long convertDateTimeToUnixTime(String dateTime, String dateFmt) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFmt);
		sdf.setTimeZone(TimeZone.getDefault());
		Date date = null;
		try {
			date = sdf.parse(dateTime);
		} catch (ParseException e) {
			throw new CommandException(e.getMessage());
		}
		return date.getTime();
	}

    protected static String dateToString(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }

}
