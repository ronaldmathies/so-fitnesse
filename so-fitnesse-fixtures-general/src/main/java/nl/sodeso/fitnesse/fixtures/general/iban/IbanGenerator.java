package nl.sodeso.fitnesse.fixtures.general.iban;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.fitnesse.ConditionalExceptionType;

import java.math.BigInteger;
import java.util.Random;

/**
 * @author Ronald Mathies
 *
 * https://nl.wikipedia.org/wiki/International_Bank_Account_Number
 */
@Fixture(name="IBAN Generator")
public class IbanGenerator {

    // Range for the accountNumberGenerator
    private static final int LOW = 100000000;
    private static final int HIGH = 999999900;

    /**
     * Constructs a new IBAN Generator.
     */
    @Start(name="startIbanGenerator", arguments = {}, example = "|start |Iban Generator|\n|note|at the moment IBAN numbers for NL and DK are supported|\n|note|Nederland (18): NLkk BBBB CCCC CCCC CK|\n|note|Denemarken (18): DKkk BBBB CCCC CCCC CC|")
    public IbanGenerator() {
    }

    /**
     * Generates an IBAN for accountNumbers between 10 and 30 numerical characters.
     *
     * @param countryCode option NL or DK
     * @param bankCode 4 alpha characters
     * @param accountNumber 10 - 30 numerical characters, for NL or DK length = 10
     * @return IBAN number as String
     */
    @Command(name = "generateIBAN", arguments = {"countryCode","bankCode", "accountNumer"}, example = "|$iban=|generateIBAN;|countryCode|bankCode|accountNumber|")
    public static String generateIBAN(String countryCode, String bankCode, String accountNumber){
        if(accountNumber.length() < 10 && accountNumber.length() > 30){
            ConditionalExceptionType.fail("The accountnumber needs to be between 10 and 30 number character long");
        }

        BigInteger numberToCheck = new BigInteger(getNumericValuesFromString(bankCode) + accountNumber + getNumericValuesFromString(countryCode) + "00");
        BigInteger modulusValue = new BigInteger("97");
        int remainder = numberToCheck.mod(modulusValue).intValue();
        String controlValue = String.valueOf(98 - remainder);

        if(controlValue.length()<2){
            return countryCode + "0" + controlValue + bankCode + accountNumber;
        } else return countryCode + controlValue + bankCode + accountNumber;
    }
    /**
     * Generates an IBAN using a 10 digit generated accountNumber that is a valid Dutch accountnumber.
     *
     * @param countryCode option NL or DK
     * @param bankCode 4 alpha characters
     * @return IBAN number as String
     */
    @Command(name = "generateIBAN", arguments = {"countryCode","bankCode"}, example = "|$iban=|generateIBAN;|countryCode|bankCode|")
    public static String generateIBAN(String countryCode, String bankCode){
        String accountNumber = generateAccountNumber();
        return generateIBAN(countryCode,bankCode,accountNumber);
    }


    /**
     * Validates a Dutch IBAN of 18 characters long
     * Using format NLkk BBBB CCCC CCCC CK:
     *   NL countryCode
     *   kk controlValue
     *   BBBB bankCode
     *   CCCC CCCC CK accountNumber (including controlvalue)
     *
     * @param iban
     * @return
     */
    @Command(name = "validateDutchIban", arguments = "iban", example = "|validateDutchIban;|iban|")
    public Boolean validateDutchIban(String iban) {
        if(iban.length() != 18){
            ConditionalExceptionType.fail("Supplied IBAN is not 18 character long.");
        }

        String countryCode = iban.substring(0, 2);
        String controlValue = iban.substring(2, 4);
        String bankCode = iban.substring(4, 8);
        String accountNumber = iban.substring(8);

        BigInteger numberToCheck = new BigInteger(getNumericValuesFromString(bankCode) + accountNumber + getNumericValuesFromString(countryCode) + controlValue);
        BigInteger modulusValue = new BigInteger("97");

        return (numberToCheck.mod(modulusValue).intValue() == 1);
    }

    public static String getNumericValueFromCharacter(String string){
        return String.valueOf(Character.getNumericValue(string.toLowerCase().charAt(0)));
    }

    public static String getNumericValuesFromString(String string){

        if (string.length() == 4){
            String bc1 = string.substring(0,1);
            String bc2 = string.substring(1,2);
            String bc3 = string.substring(2,3);
            String bc4 = string.substring(3,4);

            return getNumericValueFromCharacter(bc1) + getNumericValueFromCharacter(bc2) + getNumericValueFromCharacter(bc3) + getNumericValueFromCharacter(bc4);
        }

        if (string.length() == 2){
            String cc1 = string.substring(0,1);
            String cc2 = string.substring(1,2);

            return getNumericValueFromCharacter(cc1) + getNumericValueFromCharacter(cc2);
        } else ConditionalExceptionType.fail("This only works with a BankCode of 4 characters or a Countrycode of 2 characters, you supplied '%s' characters", string.length());
        return null; //never used
    }


    /**
     * Generates a valid accountnumber to use in an IBAN. Accountnumber is a valid Dutch accountnumber.
     *
     * @return
     * @throws ConditionalExceptionType
     */
    @Command(name = "generateAccountNumber", arguments = {}, example = "|$value=|generateAccountNumber;|")
    public static String generateAccountNumber() {
        int possibleAccountNumber = giveRandomStartingNumber();

        for (;;++possibleAccountNumber) {
            if (checkElfProef(possibleAccountNumber)) {
                return "0" + String.valueOf(possibleAccountNumber);
            }

            if (possibleAccountNumber > HIGH) {
                ConditionalExceptionType.fail("No accountNumber was found.");
            }
        }
    }

    private static int giveRandomStartingNumber() {
        return new Random().nextInt(HIGH - LOW) + LOW;
    }


    private static boolean checkElfProef(int possibleAccountNumber) {
        if (String.valueOf(possibleAccountNumber).length() == 9){
            int A, B, C, D, E, F, G, H, I, pos = 0;

            A = giveNumberOnPosition(possibleAccountNumber, pos++);
            B = giveNumberOnPosition(possibleAccountNumber, pos++);
            C = giveNumberOnPosition(possibleAccountNumber, pos++);
            D = giveNumberOnPosition(possibleAccountNumber, pos++);
            E = giveNumberOnPosition(possibleAccountNumber, pos++);
            F = giveNumberOnPosition(possibleAccountNumber, pos++);
            G = giveNumberOnPosition(possibleAccountNumber, pos++);
            H = giveNumberOnPosition(possibleAccountNumber, pos++);
            I = giveNumberOnPosition(possibleAccountNumber, pos);

            int resultaat = (9 * A) + (8 * B) + (7 * C) + (6 * D) + (5 * E) + (4 * F) + (3 * G) + (2 * H) + (1 * I);

            return resultaat % 11 == 0;
        }
        else ConditionalExceptionType.fail("Het opgegeven nummer bevat geen 9 karakters");
        return false; //never used
    }


    private static int giveNumberOnPosition(int possibleAccountNumber, int position) {
        String number = String.valueOf(possibleAccountNumber);
        String substring = number.substring(position, position + 1);
        return Integer.parseInt(substring);
    }
}

