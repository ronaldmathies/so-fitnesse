package nl.sodeso.fitnesse.fixtures.general.date;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class DateUtilFixtureTest {



    @Test
    public void testToday() throws Exception {
        DateFixture dateFixture = new DateFixture();
        assertEquals(dateFixture.resultAsString("dd-MM-yyyy"), new DateUtilFixture().today());
    }

    @Test
    public void testTodayFormat() throws Exception {
        DateFixture dateFixture = new DateFixture();
        assertEquals(dateFixture.resultAsString("MM-dd-yyyy"), new DateUtilFixture().today("MM-dd-yyyy"));
    }

    @Test
    public void testTodayMinusDays() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.subtractDays(2);
        assertEquals(dateFixture.resultAsString("dd-MM-yyyy"), new DateUtilFixture().todayMinusDays(2));
    }

    @Test
    public void testTodayMinusDaysFormat() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.subtractDays(2);
        assertEquals(dateFixture.resultAsString("MM-dd-yyyy"), new DateUtilFixture().todayMinusDays("MM-dd-yyyy", 2));
    }

    @Test
    public void testTodayPlusDays() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.addDays(2);
        assertEquals(dateFixture.resultAsString("dd-MM-yyyy"), new DateUtilFixture().todayPlusDays(2));
    }

    @Test
    public void testTodayPlusDaysFormat() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.addDays(2);
        assertEquals(dateFixture.resultAsString("MM-dd-yyyy"), new DateUtilFixture().todayPlusDays("MM-dd-yyyy", 2));
    }

    @Test
    public void testTodayPlusYears() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.addYears(2);
        assertEquals(dateFixture.resultAsString("dd-MM-yyyy"), new DateUtilFixture().todayPlusYears(2));
    }

    @Test
    public void testTodayPlusYearsFormat() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.addYears(2);
        assertEquals(dateFixture.resultAsString("MM-dd-yyyy"), new DateUtilFixture().todayPlusYears("MM-dd-yyyy", 2));
    }

    @Test
    public void testTodayMinusYears() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.subtractYears(2);
        assertEquals(dateFixture.resultAsString("dd-MM-yyyy"), new DateUtilFixture().todayMinusYears(2));
    }

    @Test
    public void testTodayMinusYearsFormat() throws Exception {
        DateFixture dateFixture = new DateFixture();
        dateFixture.subtractYears(2);
        assertEquals(dateFixture.resultAsString("MM-dd-yyyy"), new DateUtilFixture().todayMinusYears("MM-dd-yyyy", 2));
    }
}