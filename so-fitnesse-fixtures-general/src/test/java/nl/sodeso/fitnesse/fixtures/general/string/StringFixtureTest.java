package nl.sodeso.fitnesse.fixtures.general.string;

import nl.sodeso.fitnesse.CommandException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class StringFixtureTest {

    private final static String QUESTION = "How much wood would a woodchuck chuck? ";
    private final static String AWNSER = "A woodchuck would chuck as much wood as a woodchuck could chuck if a woodchuck could chuck wood";
    @Test
    public void test() {
        StringFixture fixture = new StringFixture(QUESTION);
        assertEquals("Contents is not the same.", QUESTION, fixture.result());

        fixture.appendValue(AWNSER);
        assertEquals("Contents was not appended.", QUESTION + AWNSER, fixture.result());

        String result = fixture.substring(QUESTION.length(), 1);
        assertEquals("Substring did not resulted in expected result.", "A", result);

        boolean exception = false;
        try {
            fixture.substring(-1, 10);
        } catch (CommandException commandException) {
            exception = true;
        }

        assertTrue("Exception was not thrown.", exception);

        exception = false;
        try {
            fixture.substring(0, -1);
        } catch (CommandException commandException) {
            exception = true;
        }

        assertTrue("Exception was not thrown.", exception);

        exception = false;
        try {
            fixture.substring(1, 1000);
        } catch (CommandException commandException) {
            exception = true;
        }

        assertTrue("Exception was not thrown.", exception);

        assertTrue(fixture.replaceAll("wood", "grass"));
        assertEquals("Contents is not the same.", "How much grass would a grasschuck chuck? A grasschuck would chuck as much grass as a grasschuck could chuck if a grasschuck could chuck grass", fixture.result());

        assertTrue(fixture.clearBuffer());
        assertEquals("Fixture should be cleared.", "", fixture.result());
    }

}