package nl.sodeso.fitnesse.fixtures.general.number;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class NumberUtilFixtureTest {

    @Test
    public void testAdd() throws Exception {
        int result = new NumberUtilFixture().add(10, 5);
        assertEquals(15, result);
    }

    @Test
    public void testSubtract() throws Exception {
        int result = new NumberUtilFixture().subtract(10, 5);
        assertEquals(5, result);
    }
}