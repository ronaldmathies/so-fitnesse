package nl.sodeso.fitnesse.fixtures.general.date;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class DateFixtureTest {

    @Test
    public void test() {
        DateFixture dateFixture = new DateFixture();
        dateFixture.initWithStringDate("dd-MM-yyyy", "15-02-2015");
        dateFixture.addDays(3);
        dateFixture.subtractDays(2);
        dateFixture.addMonths(3);
        dateFixture.subtractMonths(2);
        dateFixture.addYears(3);
        dateFixture.subtractYears(2);
        String result = dateFixture.resultAsString("dd-MM-yyyy");
        assertEquals("Dates do not match.", "16-03-2016", result);
    }

}