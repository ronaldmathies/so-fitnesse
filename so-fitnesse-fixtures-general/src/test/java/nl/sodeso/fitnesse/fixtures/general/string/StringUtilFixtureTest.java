package nl.sodeso.fitnesse.fixtures.general.string;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StringUtilFixtureTest {

    @Test
    public void checkIsEqual() throws Exception {
        assertTrue(new StringUtilFixture().isEqual("A", "A"));
        assertTrue(new StringUtilFixture().isEqual(null, null));
        assertFalse(new StringUtilFixture().isEqual("A", null));
        assertFalse(new StringUtilFixture().isEqual(null, "A"));
        assertFalse(new StringUtilFixture().isEqual("A", "B"));
    }

    @Test
    public void checkIsEqualIgnoreCase() throws Exception {
        assertTrue(new StringUtilFixture().isEqualIgnoreCase("a", "A"));
        assertTrue(new StringUtilFixture().isEqualIgnoreCase(null, null));
        assertFalse(new StringUtilFixture().isEqualIgnoreCase("a", null));
        assertFalse(new StringUtilFixture().isEqualIgnoreCase(null, "A"));
        assertFalse(new StringUtilFixture().isEqualIgnoreCase("a", "B"));
    }

    @Test
    public void checkIsNull() throws Exception {
        assertFalse(new StringUtilFixture().isNull("a"));
        assertTrue(new StringUtilFixture().isNull(null));

    }

    @Test
    public void checkIsNotNull() throws Exception {
        assertTrue(new StringUtilFixture().isNotNull("a"));
        assertFalse(new StringUtilFixture().isNotNull(null));
    }

    @Test
    public void checkIsNotEmpty() throws Exception {
        assertTrue(new StringUtilFixture().isNotEmpty("a"));
        assertFalse(new StringUtilFixture().isNotEmpty(""));
        assertFalse(new StringUtilFixture().isNotEmpty(null));
    }

    @Test
    public void checkIsEmpty() throws Exception {
        assertFalse(new StringUtilFixture().isEmpty("a"));
        assertTrue(new StringUtilFixture().isEmpty(""));
        assertFalse(new StringUtilFixture().isEmpty(null));
    }
}