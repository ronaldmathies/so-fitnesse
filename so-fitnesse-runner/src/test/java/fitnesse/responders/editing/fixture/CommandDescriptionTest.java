package fitnesse.responders.editing.fixture;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class CommandDescriptionTest {

    @Test
    public void testGetDescription() throws Exception {
        CommandDescription commandDescription = new CommandDescription("verifyText", new String[] {"arg1", "arg2"}, "|verifyText; |arg1|arg2|");
        assertEquals("verifyText (arg1, arg2)", commandDescription.getDescription());
        assertEquals("|verifyText; |arg1|arg2|", commandDescription.getExample());
        assertEquals("verifyText", commandDescription.getName());
        assertEquals(2, commandDescription.getArguments().length);
    }
}