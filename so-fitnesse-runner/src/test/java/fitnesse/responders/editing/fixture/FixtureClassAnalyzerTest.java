package fitnesse.responders.editing.fixture;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class FixtureClassAnalyzerTest {

    @Test
    public void testCollectFixtureClasses() throws Exception {
        Set<FixtureDescription> fixtureDescriptions = FixtureClassAnalyzer.collectCommands();
        assertTrue("No fixtures found!", !fixtureDescriptions.isEmpty());
    }

    @Test
    public void testCollectCommands() throws Exception {
        Set<FixtureDescription> fixtureDescriptions = FixtureClassAnalyzer.collectCommands();
        assertTrue("No fixtures found!", !fixtureDescriptions.isEmpty());
    }

    @Test
    public void testGetCommand() throws Exception {
        CommandDescription commandDescription = FixtureClassAnalyzer.getCommand("SampleFixture Start Sample Fixure");
        assertNotNull("Command not found.", commandDescription);
        assertNotNull("Command has no arguments.", commandDescription.getArguments());
        assertEquals("Command has an incorrect amount of arguments.", 2, commandDescription.getArguments().length);
        assertEquals("Missing 'arg0' argument.", "arg0", commandDescription.getArguments()[0]);
        assertEquals("Missing 'arg1' argument.", "arg1", commandDescription.getArguments()[1]);
    }
}