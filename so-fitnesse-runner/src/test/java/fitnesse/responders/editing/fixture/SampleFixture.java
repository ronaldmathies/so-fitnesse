package fitnesse.responders.editing.fixture;

/**
 * @author Ronald Mathies
 */
@Fixture(name = "SampleFixture")
public class SampleFixture {

    @Start(name = "Start Sample Fixure", arguments = {"arg0", "arg1"}, example = "|start |Sample Fixture|arg0|arg1|")
    public SampleFixture(String arg0, String arg1) {}

    @Command(name = "Perform", arguments = {"arg0", "arg1"}, example = "|perform; |arg0|arg1")
    public void perform(String arg0, String arg1) {}

}
