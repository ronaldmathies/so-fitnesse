package fitnesse.util;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class XmlUtil {
    private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    private static ThreadLocal<DocumentBuilder> documentBuilder = new ThreadLocal();

    public XmlUtil() {
    }

    private static DocumentBuilder getDocumentBuilder() {
        DocumentBuilder builder = (DocumentBuilder)documentBuilder.get();
        if(builder == null) {
            try {
                builder = documentBuilderFactory.newDocumentBuilder();
            } catch (ParserConfigurationException var2) {
                throw new RuntimeException(var2);
            }

            documentBuilder.set(builder);
        }

        return builder;
    }

    public static Document newDocument() {
        return getDocumentBuilder().newDocument();
    }

    public static Document newDocument(InputStream input) throws IOException, SAXException {
        return newDocument((InputSource)(new InputSource(input)));
    }

    private static Document newDocument(InputSource source) throws IOException, SAXException {
        try {
            return getDocumentBuilder().parse(source);
        } catch (SAXParseException var2) {
            throw new SAXException(String.format("SAXParseException at line:%d, col:%d, %s", new Object[]{Integer.valueOf(var2.getLineNumber()), Integer.valueOf(var2.getColumnNumber()), var2.getMessage()}));
        }
    }

    public static Document newDocument(File input) throws IOException, SAXException {
        try {
            return getDocumentBuilder().parse(new InputSource(new InputStreamReader(new FileInputStream(input), "UTF-8")));
        } catch (SAXParseException var2) {
            throw new SAXException(String.format("SAXParseException at %s:%d,%d: %s", new Object[]{input.getCanonicalPath(), Integer.valueOf(var2.getLineNumber()), Integer.valueOf(var2.getColumnNumber()), var2.getMessage()}));
        }
    }

    public static Document newDocument(String input) throws IOException, SAXException {
        return newDocument((InputSource)(new InputSource(new StringReader(input))));
    }

    public static Element getElementByTagName(Element element, String name) {
        NodeList nodes = element.getElementsByTagName(name);
        return nodes.getLength() == 0?null:(Element)nodes.item(0);
    }

    public static Element getLocalElementByTagName(Element context, String tagName) {
        NodeList childNodes = context.getChildNodes();

        for(int i = 0; i < childNodes.getLength(); ++i) {
            Node node = childNodes.item(i);
            if(node instanceof Element && tagName.equals(node.getNodeName())) {
                return (Element)node;
            }
        }

        return null;
    }

    public static String getTextValue(Element element, String name) {
        Element namedElement = getElementByTagName(element, name);
        return getElementText(namedElement);
    }

    public static String getLocalTextValue(Element element, String name) {
        Element namedElement = getLocalElementByTagName(element, name);
        return getElementText(namedElement);
    }

    public static String getElementText(Element namedElement) {
        if(namedElement == null) {
            return null;
        } else {
            String text = namedElement.getTextContent();
            return text.length() == 0?null:text;
        }
    }

    public static void addTextNode(Element element, String tagName, String value) {
        if(value != null && !value.equals("")) {
            Element titleElement = element.getOwnerDocument().createElement(tagName);
            titleElement.setTextContent(value);
            element.appendChild(titleElement);
        }

    }

    public static void addCdataNode(Element element, String tagName, String value) {
        if(value != null && !value.equals("")) {
            Document document = element.getOwnerDocument();
            Element titleElement = document.createElement(tagName);
            titleElement.appendChild(document.createCDATASection(value));
            element.appendChild(titleElement);
        }

    }

    public static String xmlAsString(Document doc) throws IOException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        // Ronald Mathies: BUGFIX IVM Java 1.7, indent-number is niet valide.
//        transformerFactory.setAttribute("indent-number", new Integer(2));
        StringWriter sw = new StringWriter();

        try {
            Transformer e = transformerFactory.newTransformer();
            e.setOutputProperty("encoding", "UTF-8");
            e.setOutputProperty("standalone", "yes");
            e.setOutputProperty("indent", "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(sw);
            e.transform(source, result);
        } catch (TransformerException var6) {
            throw new RuntimeException("Arjan should catch this", var6);
        }

        return sw.toString();
    }
}
