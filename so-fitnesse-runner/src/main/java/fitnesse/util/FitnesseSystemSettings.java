package fitnesse.util;

/**
 * @author Ronald Mathies
 */
public class FitnesseSystemSettings {

    public static Boolean SYSTEM_FITNESSE_STOP_TEST_KEY = false;  // property is set using stopFitnesseTestOnException from the OperationsFixture. Can be set on a per-testcondition basis
    private static final String SYSTEM_FITNESSE_SKIP_TEST_KEY = "fitnesseSkipTest";

    public static boolean stopTestOnException() {
        if (SYSTEM_FITNESSE_STOP_TEST_KEY == null) {
            return false;
        }
        return SYSTEM_FITNESSE_STOP_TEST_KEY;
    }


    public static boolean executeTests() {
        String property = System.getProperty(SYSTEM_FITNESSE_SKIP_TEST_KEY);
        if (property == null) {
            return false;
        }

        return !Boolean.valueOf(property);
    }

}
