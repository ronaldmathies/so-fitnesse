package fitnesse.responders.editing.fixture;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Ronald Mathies
 */
public class FixtureClassAnalyzer {

    private static ArrayList<FixtureDescription> cache = new ArrayList<FixtureDescription>();

    public static ArrayList<FixtureDescription> collectFixtureClasses() {
        if (cache.isEmpty()) {
            Reflections reflections = new Reflections(
                    new ConfigurationBuilder()
                            .setUrls(ClasspathHelper.forJavaClassPath())
                            .setScanners(new SubTypesScanner(), new TypeAnnotationsScanner())
            );

            Set<Class<?>> fixtureClasses = reflections.getTypesAnnotatedWith(Fixture.class);

            for (Class<?> fixtureClass : fixtureClasses) {
                Fixture fixture = fixtureClass.getAnnotation(Fixture.class);
                FixtureDescription fixtureDescription = new FixtureDescription(fixture.name());
                cache.add(fixtureDescription);

                for (Constructor<?> constructor : fixtureClass.getConstructors()) {
                    if (constructor.isAnnotationPresent(Start.class)) {
                        Start start = constructor.getAnnotation(Start.class);
                        fixtureDescription.addCommand(new CommandDescription(start.name(), start.arguments(), start.example()));
                    }
                }

                for (Method method : fixtureClass.getMethods()) {
                    if (method.isAnnotationPresent(Command.class)) {
                        Command command = method.getAnnotation(Command.class);
                        fixtureDescription.addCommand(new CommandDescription(command.name(), command.arguments(), command.example()));
                    }
                }

            }
        }

        return cache;
    }

    public static Set<FixtureDescription> collectCommands() {
        TreeSet<FixtureDescription> fixtureDescriptions = new TreeSet<FixtureDescription>(new Comparator<FixtureDescription>() {
            @Override
            public int compare(FixtureDescription k1, FixtureDescription k2) {
                return k1.getName().compareTo(k2.getName());
            }
        });

        fixtureDescriptions.addAll(collectFixtureClasses());

        return fixtureDescriptions;
    }

    public static CommandDescription getCommand(String command) {
        ArrayList<FixtureDescription> fixtureDescriptions = collectFixtureClasses();
        for (FixtureDescription fixtureDescription : fixtureDescriptions) {

            if (command.startsWith(fixtureDescription.getName())) {
                for (CommandDescription commandDescription : fixtureDescription.getCommands()) {
                    if (command.endsWith(commandDescription.getName())) {
                        return commandDescription;
                    }
                }
            }
        }

        return null;
    }

}
