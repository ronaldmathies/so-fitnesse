package fitnesse.responders.editing.fixture;

/**
 * @author Ronald Mathies
 */
public class CommandDescription {

    private String name;
    private String[] arguments;
    private String example;

    public CommandDescription(String name, String[] arguments, String example) {
        this.name = name;
        this.arguments = arguments;
        this.example = example;
    }

    /**
     * Name of the fixture method.
     * @return name of the fixture method.
     */
    public String getName() {
        return this.name;
    }

    /**
     * List of arguments to pass in.
     * @return the list of arguments to pass in.
     */
    public String[] getArguments() {
        return this.arguments;
    }

    /**
     * A sample slim table row usage of this fixture method.
     * @return a sample slim table row usage of this fixture method.
     */
    public String getExample() {
        return this.example;
    }

    /**
     * Describes the fixture method visually for the user.
     * @return description of the fixture method.
     */
    public String getDescription() {
        StringBuilder description = new StringBuilder(getName());

        if (arguments != null && arguments.length > 0) {
            description.append(" (");
            for (int index = 0; index < arguments.length; index++) {
                description.append(arguments[index]);

                if (index < arguments.length - 1) {
                    description.append(", ");
                }
            }
            description.append(")");
        } else {
            description.append(" ( no arguments ) ");
        }

        return description.toString();
    }
}
