package fitnesse.responders.editing.fixture;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class FixtureDescription {

    private String name;

    private List<CommandDescription> commands = new ArrayList<>();

    public FixtureDescription(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void addCommand(CommandDescription commandDescription) {
        this.commands.add(commandDescription);
    }

    public List<CommandDescription> getCommands() {
        return this.commands;
    }
}
