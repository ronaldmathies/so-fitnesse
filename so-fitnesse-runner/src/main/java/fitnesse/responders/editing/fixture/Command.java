package fitnesse.responders.editing.fixture;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Command annotation can be used to mark a method as a fixture method.
 *
 * Use this annotation to display the method in the list of fixture methods in Fitnesse.
 *
 * @author Ronald Mathies
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {

    /**
     * Name of the fixture method.
     * @return name of the fixture method.
     */
    String name();

    /**
     * List of arguments to pass in.
     * @return the list of arguments to pass in.
     */
    String[] arguments();

    /**
     * A sample slim table row usage of this fixture method.
     * @return a sample slim table row usage of this fixture method.
     */
    String example();
}
