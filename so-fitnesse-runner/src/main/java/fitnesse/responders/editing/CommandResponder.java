package fitnesse.responders.editing;

import fitnesse.FitNesseContext;
import fitnesse.authentication.InsecureOperation;
import fitnesse.authentication.SecureOperation;
import fitnesse.authentication.SecureResponder;
import fitnesse.http.Request;
import fitnesse.http.Response;
import fitnesse.http.SimpleResponse;
import fitnesse.responders.NotFoundResponder;
import fitnesse.responders.editing.fixture.CommandDescription;
import fitnesse.responders.editing.fixture.FixtureClassAnalyzer;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * @author Ronald Mathies
 */
public class CommandResponder implements SecureResponder {

    public Response makeResponse(FitNesseContext context, Request request) throws UnsupportedEncodingException {
        String source = request.getResource();

        String command = URLDecoder.decode(source, "UTF-8");
        CommandDescription commandDescription = FixtureClassAnalyzer.getCommand(command);

        Response response;
        if (commandDescription != null) {
            response = responseWith(commandDescription.getExample());
        } else {
            response = responseWith("Fixture: " + command + " not found.");
        }

        return response;
    }

    protected Response pageNotFoundResponse(FitNesseContext context, Request request) {
        return new NotFoundResponder().makeResponse(context, request);
    }

    protected Response responseWith(String content) {
        SimpleResponse response = new SimpleResponse();
        response.setContentType(getContentType());
        response.setContent(content);
        return response;
    }

    protected String getContentType() {
        return Response.Format.HTML.getContentType();
    }

    public SecureOperation getSecureOperation() {
        return new InsecureOperation();
    }
}

