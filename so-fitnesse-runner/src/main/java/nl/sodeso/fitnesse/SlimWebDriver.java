package nl.sodeso.fitnesse;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.thoughtworks.selenium.webdriven.JavascriptLibrary;
import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.commons.fileutils.FileFinder;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Slim Web Driver")
public class SlimWebDriver {

    private static final String REGEXPI_SYNTAX = "regexpi:";

    private static final String LOCATOR_KPATH = "xpath=";
    private static final String LOCATOR_XPATH = "xpath=";
    private static final String LOCATOR_CSS = "css=";
    private static final String LOCATOR_ID = "id=";
    private static final String LOCATOR_NAME = "name=";
    private static final String LOCATOR_LINKTEXT = "link=";
    private static final String LOCATOR_PARTIAlLINKTEXT = "partiallink=";

    private static final String MAC_OS_NAME="mac";

    public final static String BROWSER_FIREFOX = "firefox";
    public final static String BROWSER_CHROME = "chrome";
    public final static String BROWSER_SAFARI = "safari";
    public final static String BROWSER_INTERNET_EXPLORER = "internet explorer";
    public final static String BROWSER_HEADLESS = "headless";
    public final static String BROWSER_REMOTE = "remote";
    public static final String ELEMENT_INPUT = "input";
    public static final String ELEMENT_TEXTAREA = "textarea";
    public static final String ATTR_TYPE = "type";
    public static final String TYPE_CHECKBOX = "checkbox";
    public static final String TYPE_RADIO = "radio";
    public static final String ATTR_VALUE = "value";

    private WebDriver driver = null;
    private String screenshotLocation = null;
    private int seconds;
    
    public static String SCREENSHOTLOCATION;
    public static FirefoxProfile remoteProfile;

    private Keys ctrlOrCmd = Keys.CONTROL;

    @Start(name="Start Slim Web Driver", arguments = {"configuration"}, example = "|start |Slim Web Driver|selenium.properties")
    public SlimWebDriver(String configuration) {
        initDriver(configuration);
        setCmdKeyifMacOs();
    }
    
    /**
     * Will check is the OS name starts with 'mac'. If this is the case the COMMAND key  will be used instead of the CONTROL key if 'CtrlOrCmd' is used.
     */
    private void setCmdKeyifMacOs() {
        if (System.getProperty("os.name").toLowerCase().startsWith(MAC_OS_NAME)) {
            ctrlOrCmd = Keys.COMMAND;
        }
    }

    /**
     * Creates a screen shot of the current browser interface and stores it into the predefined location.
     *
     * @param filename the filename of the screen shot.
     * @return a URL referencing the screen shot location.
     */
    @Command(name = "saveScreenshot", arguments = {"filename"}, example = "|show|saveScreenshot;|filename.png|")
    public String saveScreenshot(String filename) {

        File screenshotSaveLocation = new File(new File(screenshotLocation), filename);
        Screenshot screenshot = new AShot().takeScreenshot(driver);
        try {
            ImageIO.write(screenshot.getImage(), "PNG", screenshotSaveLocation);
        } catch (IOException e) {
            throw new CommandException(String.format("Unable to save screenshot to '%s'", screenshotSaveLocation.getAbsolutePath()));
        }

        return String.format("<a href=\"%s\"><img src=\"%s\" width=\"300\"/></a>", screenshotSaveLocation.getAbsolutePath(), screenshotSaveLocation.getAbsolutePath());
    }

    /**
     * Register the default timeout that will apply to all commands.
     *
     * @param seconds the amount of seconds to wait until the test fails.
     * @throws CommandException when the specified seconds is a negative number.
     */
    @Command(name="defaultCommandTimeout", arguments = {"seconds"}, example = "|defaultCommandTimeout; |10|")
    public void defaultCommandTimeout(int seconds) {
        if (seconds < 1) {
            ConditionalExceptionType.fail("Unable to set a negative amount of seconds as the default command timeout.");
        }

        this.seconds = seconds;

        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    /**
     * Register the default timeout that will apply to page loading actions.
     *
     * @param seconds the amount of seconds to wait until the page loads
     * @throws CommandException when the specified seconds is a negative number.
     */
    @Command(name="defaultPageLoadTimeout", arguments = {"seconds"}, example = "|defaultPageLoadTimeout; |10|")
    public void defaultPageLoadTimeout(int seconds) {
        if (seconds < 1) {
            ConditionalExceptionType.fail("Unable to set a negative amount of seconds as the default page load timeout.");
        }

        driver.manage().timeouts().pageLoadTimeout(seconds, TimeUnit.SECONDS);
    }

    /**
     * Deletes all cookies.
     */
    @Command(name="deleteAllCookies", arguments = {}, example = "|deleteAllCookies; |")
    public boolean deleteAllCookies() {
        driver.manage().deleteAllCookies();
        return true;
    }

    /**
     * Delete the cookie with the specified name.
     *
     * @param name the name of the cookie.
     */
    @Command(name="deleteCookieByName", arguments = {"name"}, example = "|deleteCookieByName; |name|")
    public boolean deleteCookieByName(String name) {
        driver.manage().deleteCookieNamed(name);
        return true;
    }

    /**
     * Checks if a cookie exists with the specified <code>name</code>.
     *
     * @param name the name of the cookie.
     * @throws CommandException When the cookie with the specified name does not exist.
     */
    @Command(name="checkIfCookieExists", arguments = {"name"}, example = "|deleteCookieByName; |name|")
    public boolean checkIfCookieExists(String name) {
        boolean found = false;
        for (Cookie cookie : driver.manage().getCookies()) {
            if (cookie.getName().equalsIgnoreCase(name)) {
                found = true;
            }
        }

        if (found) {
            return true;
        }

        ConditionalExceptionType.fail("Cookie with name '%s' does not exist.", name);
    }


    /**
     * Open a new window with the given <code>url</code>
     *
     * @param url the url to call upon opening the new window.
     *
     * @return true if the website could be opened.
     * @throws CommandException when the specified URL is not valid.
     */
    @Command(name="openWebsite", arguments = {"url"}, example = "|openWebsite; |http://localhost:8080|")
    public boolean openWebsite(String url) {
        try {
            new URL(url);
        } catch(MalformedURLException e) {
            ConditionalExceptionType.fail("The specified URL '%s' is not a valid URL.", url);
        }

        driver.get(url);

        return true;
    }

    /**
     * Maximizes the window
     */
    @Command(name = "windowMaximize", arguments = {}, example = "|windowMaximize;|")
    public void windowMaximize() {
        driver.manage().window().maximize();
    }

    /**
     * Resizes the window to the specified dimensions
     */
    @Command(name = "windowResize", arguments = {"pixelsHorizontal", "pixelsVertical"}, example = "|windowResize;|pixelsWidth|pixelsHeight|")
    public void windowResize(int pixelsWidth, int pixelsHeight) {
        driver.manage().window().setSize(new Dimension(pixelsWidth, pixelsHeight));
    }

    /**
     * Moves the window
     *
     * @param horizontal shift in pixels
     * @param vertical   shift in pixels
     */

    @Command(name = "windowMove", arguments = {"horizontal", "vertical"}, example = "|windowMove;|100|200|")
    public void windowMove(int horizontal, int vertical) {
        int xPos = driver.manage().window().getPosition().getX() + horizontal;
        int yPos = driver.manage().window().getPosition().getY() + vertical;

        driver.manage().window().setPosition(new Point(xPos, yPos));
    }

    /**
     * Sets the window position
     */
    @Command(name = "windowSetPosition", arguments = {"x", "y"}, example = "|windowSetPosition|x in pixels|y in pixels|")
    public void windowSetPosition(int x, int y) {
        driver.manage().window().setPosition(new Point(x, y));
    }

    /**
     * Quit the browser.
     */
    @Command(name="closeWebsite", arguments = {}, example = "|closeWebsite; |")
    public void closeWebsite() {
        driver.quit();
    }

    /**
     * Pushes a new frame as the current driver.
     * @param locator the locator to use to find the web element.
     */
    @Command(name="switchToFrame", arguments = {"locator"}, example = "|switchToFrame; |locator|")
    public void switchToFrame(String locator) {
        WebDriver drv = this.driver.switchTo().frame(findElement(locator));
        if (drv == null) {
            ConditionalExceptionType.fail("No frame found matching the locator '%s'.", locator);
        }
        this.driver = drv;
    }

    /**
     * Go back to the initial state.
     */
    @Command(name="switchToStart", arguments = {}, example = "|switchToStart; |")
    public void switchToStart() {
        driver = driver.switchTo().defaultContent();
    }

    /**
     * Pushes a new window as the current driver.
     * @param nameOrHandle the name or id attribute.
     */
    @Command(name="switchToWindow", arguments = {"nameOrHandle"}, example = "|switchToWindow; |nameOrHandle|")
    public void switchToWindow(String nameOrHandle) {
        WebDriver drv = this.driver.switchTo().window(nameOrHandle);
        if (drv == null) {
            ConditionalExceptionType.fail("No window found where the name or handle corresponds with the value '%s'", nameOrHandle);
        }
        this.driver = drv;
    }
    
    /**
     * Switches to the opened window that is not the curent window for webdriver.
     */
    @Command(name="switchToUnfocussedWindow", arguments = {}, example = "|switchToUnfocussedWindow; |")
    public void switchToUnfocussedWindow() {
        String currentHandle = driver.getWindowHandle();
        Set<String> allHandles = driver.getWindowHandles();
        allHandles.remove(currentHandle);

        if (!allHandles.isEmpty()) {
            driver.switchTo().window(allHandles.iterator().next());
        }
    }

    /**
     * Returns the current windowhandle
     */
    @Command(name = "getWindowHandle", arguments = {}, example = "|$handle=| getWindowHandle;|")
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    /**
     * Deprecated command, should wait for the page to load but is now implicit.
     */
    @Command(name="waitForPageLoad (Deprecated, timeout implicit)", arguments = {}, example = "|waitForPageLoad; |")
    public void waitForPageLoad() {}

    /**
     * Refreshes the current web site..
     */
    @Command(name="refreshWebsite", arguments = {}, example = "|refreshWebsite; |")
    public void refreshWebsite() {
        driver.navigate().refresh();
    }

    /**
     * Presses the browser back button.
     */
    @Command(name="browserBack", arguments = {}, example = "|browserBack; |")
    public void browserBack() {
        driver.navigate().back();
    }

    /**
     * Presses the browser back button.
     */
    @Command(name="browserForward", arguments = {}, example = "|browserForward; |")
    public void browserForward() {
        driver.navigate().forward();
    }

    /**
     * Checks if the title of the website matches the title supplied.
     *
     * @param title the title of the website.
     *
     * @return true if it matches.
     * @throws CommandException when the element can not be found or the value does not match.
     */
    @Command(name="checkWebsiteTitle", arguments = {"title"}, example = "|checkWebsiteTitle;|title|")
    public boolean checkWebsiteTitle(String title) {
        if (driver.getTitle().equals(title)) {
            return Boolean.TRUE;
        }

        ConditionalExceptionType.fail("Website title does not match, expected '%s', found '%s'.", title, driver.getTitle());
    }

    /**
     * Pause is not allowed anymore.
     */
    @Command(name="pause (deprecated, does not work)", arguments = {"milliseconds"}, example = "|pause;|milliseconds|")
    public void pause(String milliseconds) {
    }

    /**
     * Pause is not allowed anymore.
     */
    @Command(name="waitForSeconds (deprecated, does not work)", arguments = {"seconds"}, example = "|waitForSeconds; |seconds|")
    public void waitForSeconds(String milliseconds) {
    }

    /**
     * Clicks on an element in the page.
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element can not be found.
     */
    @Command(name="click", arguments = {"locator"}, example = "|click; |//path/to/element (of css=, id=, name=)|")
    public boolean click(String locator) {
        findElement(locator).click();
        return true;
    }


    /**
     * Doubleclicks on an element in the page
     *
     * @param locator the locator to use to find the element
     * @throws CommandException when the element can not be found
     */
    @Command(name="doubleClick", arguments = {"locator"}, example = "|doubleClick; |//path/to/element (of css=, id=, name=)|")
    public boolean doubleClick(String locator) {
        WebElement element = findElement(locator);
        Actions action = new Actions(driver);
        action.doubleClick(element);
        action.perform();

        return true;
    }
    
        /**
     * Richt click / context click action
     *
     * @param locator the locator to use to find the element
     * @throws ConditionalExceptionType when the element can not be found
     */
    @Command(name = "contextClick", arguments = {"locator"}, example = "|contextClick; |//path/to/element (of css=, id=, name=)|")
    public boolean contextClick(String locator) {
        WebElement element = findElement(locator);
        Actions action = new Actions(driver);
        action.contextClick(element);
        action.perform();

        return true;
    }
    
    /**
     * Clicks on an element in the page that is not visible i.e. hidden
     *
     * @param locator the locator to use to find the element
     */
    @Command(name = "clickUsingJavascript", arguments = {"locator"}, example = "|clickUsingJavascript; |//path/to/element (of css=, id=, name=)|")
    public boolean clickUsingJavascript(String locator) {
        WebElement element = findElement(locator);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", element);

        return true;
    }

    /**
     * Performs a pause action using the Actions class
     *
     * @param millis the time to pause in milliseconds
     *
     * More information:
     *
     * <a href="http://selenium.googlecode.com/git/docs/api/java/org/openqa/selenium/interactions/Actions.html">Actions</a>
     *
     * NOTE: Use this only when it is neccesary, using sleep commands is not a recommended action while using WebDriver.
     */
    @Command(name="sleep (deprecated, works but may be removed in a future version)", arguments = {"milliseconds"}, example = "|sleep; | milliseconds |")
    public boolean sleep(long millis) {
        Actions action = new Actions(driver);
        action.pause(millis);
        action.perform();

        return true;
    }

    /**
     * Clicks on an element in the page at the specified location.
     *
     * @param locator the locator to use to find the element.
     * @param coordinates the coordinate
     * @throws CommandException when the element can not be found.
     */
    @Command(name="clickAt (Deprecated, does not work))", arguments = {"locator"}, example = "|click; |//path/to/element (of css=, id=, name=)|")
    public boolean clickAt(String locator, String coordinates) {
        ConditionalExceptionType.fail("The clickAt function does not exist anymore.");
    }

    /**
     * Clicks on an element in the page to trigger a page load.
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element can not be found.
     */
    @Command(name="clickAndWait (Deprecated, timeout implicit)", arguments = {"locator"}, example = "|clickAndWait; |//path/to/element (of css=, id=, name=)|")
    public boolean clickAndWait(String locator) {
        return click(locator);
    }

    /**
     * Clicks on an element in the page for a certain number of times..
     *
     * @param locator the locator to use to find the element.
     * @param numberOfTimesToExecute the number of times to click the element.
     * @throws CommandException when the element can not be found.
     */
    @Command(name="clickUpToTimes", arguments = {"locator", "times"}, example = "|clickUpToTimes; |//path/to/element (of css=, id=, name=)|times|")
    public boolean clickUpToTimes(String locator, String numberOfTimesToExecute) {
        int totalNumberOfTimes = Integer.parseInt(numberOfTimesToExecute);
        for (int times = 1; times <= totalNumberOfTimes; times++) {
            findElement(locator).click();
        }
        return true;
    }

    /**
     * Focus is nothing more then a click on an element, this is deprecated.
     *
     * @param locator the locator to use to find the element.
     */
    @Deprecated
    @Command(name="focus (deprecated)", arguments = {"locator"}, example = "|focus; |//path/to/element (of css=, id=, name=)|")
    public boolean focus(String locator) {
        return click(locator);
    }

    /**
     * Checks a checkbox (or radiobutton), if it was checked then nothing will be done.
     *
     * @param locator the locator to use to find the element.
     */
    @Command(name="makeChecked", arguments = {"locator"}, example = "|makeChecked; |//path/to/element (of css=, id=, name=)|")
    public boolean makeChecked(String locator) {
        WebElement element = findElement(locator);

        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT)) {
            String type = element.getAttribute(ATTR_TYPE);
            if (type.equalsIgnoreCase(TYPE_CHECKBOX) || type.equalsIgnoreCase(TYPE_RADIO)) {
                if (!element.isSelected()) {
                    element.click();
                }
            } else {
                throwConditionalExceptionType.fail("Element found using the locator '%s' is not a checkbox or a radio button, makeChecked only works on a checkbox or radio button.");
            }
        } else {
            ConditionalExceptionType.fail("Element found using the locator '%s' is not an input element but a '%s' element.", locator, tagName);
        }
        return true;
    }

    /**
     * Unchecks a checkbox (or radiobutton), if it was checked then nothing will be done.
     *
     * @param locator the locator to use to find the element.
     */
    @Command(name="makeNotChecked", arguments = {"locator"}, example = "|makeNotChecked; |//path/to/element (of css=, id=, name=)|")
    public boolean makeNotChecked(String locator) {
        WebElement element = findElement(locator);
        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT)) {
            String type = element.getAttribute(ATTR_TYPE);
            if (type.equalsIgnoreCase(TYPE_CHECKBOX)) {
                if (!element.isSelected()) {
                    element.click();
                }
            } else {
                ConditionalExceptionType.fail("Element found using the locator '%s' is not a checkbox, makeNotChecked only works on a checkbox.");
            }
        } else {
            ConditionalExceptionType.fail("Element found using the locator '%s' is not an input element but a '%s' element.", locator, tagName);
        }

        return true;
    }

    /**
     * Writes the specified value to the element found using the locator.
     *
     * @param locator the locator to use to find the element.
     * @param value the value to write to the element.
     *
     * @return true if successful
     * @throws CommandException when the element can not be found or is not an INPUT / TEXTAREA element.
     */
    @Command(name="type", arguments = {"locator", "value"}, example = "|type; |//path/to/input (of css=, id=, name=)|value|")
    public boolean type(String locator, String value) {
        WebElement element = findElement(locator);
        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT) || tagName.equalsIgnoreCase(ELEMENT_TEXTAREA)) {
            element.sendKeys(Keys.chord(ctrlOrCmd, "a"), value);
            return true;
        }

        ConditionalExceptionType.fail("Element found using the locator '%s' is not an input or textarea element but a '%s' element.", locator, tagName);
    }

    /**
     * Verifies if the text at the specified locator is compliant with the given pattern.
     *
     * If the locator refers to an input element it will check if it is a checkbox or radio button, if so it will use "true" or "false", if it
     * is any other input element it will use the value of the attribute "value".
     *
     * @param locator the locator to use to find the element.
     * @param pattern the pattern.
     */
    @Command(name="verifyText", arguments = {"locator", "pattern"}, example = "|verifyText; |//path/to/input (of css=, id=, name=)|pattern|")
    public boolean verifyText(String locator, String pattern) {
        WebElement element = findElement(locator);
        String value = element.getText();

        if (pattern.startsWith(REGEXPI_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEXPI_SYNTAX.length()), Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
            return compiledPattern.matcher(value).matches();
        } else {
            return value.matches(pattern);
        }
    }


    /**
     * Returns the text that is present in the specified locator.
     *
     * @param locator the locator to use to find the element.
     */
    @Command(name = "getText", arguments = {"locator"}, example = "|getText; |//path/to/input (of css=, id=, name=)|")
    public String getText(String locator) {
        WebElement element = findElement(locator);

        return element.getText();
    }

    /**
     * Verifies if the text at the specified locator is compliant with the given pattern.
     *
     * If the locator refers to an input element it will check if it is a checkbox or radio button, if so it will use "true" or "false", if it
     * is any other input element it will use the value of the attribute "value".
     *
     * @param locator the locator to use to find the element.
     * @param pattern the pattern.
     */
    @Command(name="verifyValue", arguments = {"locator", "pattern"}, example = "|verifyValue; |//path/to/input (of css=, id=, name=)|pattern|")
    public boolean verifyValue(String locator, String pattern) {
        WebElement element = findElement(locator);
        String value = getValueElement(element);

        if (pattern.startsWith(REGEXPI_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEXPI_SYNTAX.length()), Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
            return compiledPattern.matcher(value).matches();
        } else {
            return value.matches(pattern);
        }
    }

    /**
     * Returns the value of an attribute at a specific location
     *
     * @param locator the locator to find the element on the page.
     * @param attribute the name of the attribute.
     */
    @Command(name="getAttribute", arguments = {"locator", "attribute"}, example = "|$value= |getAttribute; |//path/to/input (of css=, id=, name=)|attribute|")
    public String getAttribute(String locator, String attribute) {
        WebElement element = findElement(locator);
        return element.getAttribute(attribute);
    }

    /**
     * Verifies if the given text is present anywhere in the source of the page.
     *
     * @param text the value to verify.
     */
    @Command(name="verifyTextPresent", arguments = {"text"}, example = "|verifyTextPresent; |text|")
    public boolean verifyTextPresent(String text) {
        return driver.getPageSource().contains(text);
    }

    /**
     * Verifies if the given text is not present anywhere in the source of the page.
     *
     * @param text the value to verify.
     */
    @Command(name="verifyTextNotPresent", arguments = {"text"}, example = "|verifyTextNotPresent; |text|")
    public boolean verifyTextNotPresent(String text) {
        return !driver.getPageSource().contains(text);
    }

    /**
     * Waits for an element to become editable.
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element was not found or the element did not become editable.
     */
    @Command(name="waitForEditable", arguments = {"locator"}, example = "|waitForEditable; |//path/to/input (of css=, id=, name=)|")
    public boolean waitForEditable(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(element));
            if (webElement == null) {
                ConditionalExceptionType.fail("Element with locator '%s' dit not become editable within '%d' seconds.", locator);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Element with locator '%s' dit not become editable within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Waits for an element to become not editable.
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element was not found or the element did  not become un-editable.
     */
    @Command(name="waitForNotEditable", arguments = {"locator"}, example = "|waitForNotEditable; |//path/to/input (of css=, id=, name=)|")
    public boolean waitForNotEditable(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean notClickable = wait.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(element)));
            if (!notClickable) {
                ConditionalExceptionType.fail("Element with locator '%s' dit not become un-editable within '%d' seconds.", locator);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Element with locator '%s' dit not become un-editable within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Waits for an element to become present in the dom tree.
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element was not found or the element did not appear in the dom tree.
     */
    @Command(name="waitForElementPresent", arguments = {"locator"}, example = "|waitForElementPresent; |//path/to/input (of css=, id=, name=)|")
    public boolean waitForElementPresent(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            WebElement webElement = wait.until(ExpectedConditions.presenceOfElementLocated(constructLocatorFromString(locator)));
            if (webElement == null) {
                ConditionalExceptionType.fail("Element with locator '%s' dit not become present within '%d' seconds", locator);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Element with locator '%s' dit not become present within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Check that the element is not present
     *
     * @param locator the locator to find the element.
     * @return true if there are no elements present with the specified locator.
     */
    @Command(name = "verifyElementNotPresent", arguments = "locator", example = "|verifyElementNotPresent; |//path/to/input (of css=, id=, name=)|")
    public boolean verifyElementNotPresent(String locator){
        return driver.findElements(constructLocatorFromString(locator)).isEmpty();
    }

    /**
     * Waits for an element to become detached from the dom tree.
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element was not found or the element did not detach itself from the dom tree.
     */
    @Command(name="waitForElementNotPresent", arguments = {"locator"}, example = "|waitForElementNotPresent; |//path/to/input (of css=, id=, name=)|")
    public boolean waitForElementNotPresent(String locator) {
        WebElement element = findElement(locator);

        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean stale = wait.until(ExpectedConditions.stalenessOf(element));
            if (!stale) {
                ConditionalExceptionType.fail("Element with locator '%s' dit not become absent within '%d' seconds", locator);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Element with locator '%s' dit not become absent within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Waits for an element to become visible (meaning visible and width and height != 0).
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element was not found or the element did not become visible.
     */
    @Command(name="waitForVisible", arguments = {"locator"}, example = "|waitForVisible; |//path/to/input (of css=, id=, name=)|")
    public boolean waitForVisible(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            WebElement webElement = wait.until(ExpectedConditions.visibilityOf(element));
            if (webElement.isDisplayed()) {
                ConditionalExceptionType.fail("Element with locator '%s' dit not become visible (visible but also width and height != 0) within '%d' seconds.", locator, seconds);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Element with locator '%s' dit not become visible (visible but also width and height != 0) within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Wait for a basic authentication dialog.
     *
     * @param username username.
     * @param password password.
     *
     * @throws CommandException when a timeout occurs, a webdriver exception occurs or when the value is not appearing.
     */
    @Command(name="waitForBasicAuthentication", arguments = {"username", "password"}, example = "|waitForBasicAuthentication; |username|password|")
    public boolean waitForBasicAuthentication(String username, String password) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            alert.authenticateUsing(new UserAndPassword(username, password));
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Timeout while waiting for basic authentication dialog.");
        } catch (WebDriverException webdriverException) {
            ConditionalExceptionType.fail("WebDriver exception while waiting for basic authentication dialog.");
        }

        return true;
    }

    /**
     * Wait for an alert dialog and when it appears it will accept it.
     *
     * @throws CommandException when a timeout occurs, a webdriver exception occurs or when the dialog is not appearing.
     */
    @Command(name="waitForAlertAndAccept", arguments = {}, example = "|waitForAlertAndAccept; |")
    public boolean waitForAlertAndAccept() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            alert.accept();
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Timeout while waiting for message dialog.");
        } catch (WebDriverException webdriverException) {
            ConditionalExceptionType.fail("WebDriver exception while waiting for message dialog.");
        }

        return true;
    }

    /**
     * Wait for an alert dialog and when it appears it will dismiss it.
     *
     * @throws CommandException when a timeout occurs, a webdriver exception occurs or when the dialog is not appearing.
     */
    @Command(name="waitForAlertAndDismiss", arguments = {}, example = "|waitForAlertAndDismiss; |")
    public boolean waitForAlertAndDismiss() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            alert.dismiss();
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Timeout while waiting for message dialog.");
        } catch (WebDriverException webdriverException) {
            ConditionalExceptionType.fail("WebDriver exception while waiting for message dialog.");
        }

        return true;
    }


    /**
     * Triggers the specified event on the specified element.
     *
     * @param locator the locator to use to find the element.
     * @param event the event to fire.
     * @throws CommandException when the event failed to fire.
     */
    @Command(name="fireEventAndWait", arguments = {"locator", "event"}, example = "|fireEventAndWait; |//path/to/input (of css=, id=, name=)|event (for example 'blur')|")
    public boolean fireEventAndWait(String locator, String event) {
        try {
            WebElement element = findElement(locator);
            JavascriptLibrary javascript = new JavascriptLibrary();
            javascript.callEmbeddedSelenium(driver, "triggerEvent", element, event);
        } catch (Exception e) {
            ConditionalExceptionType.fail("Failed to fire event '%s' on element with locator '%s'", event, locator);
        }

        return true;
    }

    /**
     * Executes the specified javascript in the browser.
     *
     * @param script the javascript to execute.
     * @throws CommandException when the javascript failed to execute.
     */
    @Command(name="executeJavaScript", arguments = {"script"}, example = "|executeJavaScript; |javascript (for example: alert('Hello World!');|")
    public boolean executeJavaScript(String script) {
        try {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript(script);
        } catch (Exception e) {
            ConditionalExceptionType.fail("Failed to execute javascript.");
        }

        return true;
    }

    /**
     * Waits for an element to become invisible (meaning visible and width and height != 0).
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the element was not found or the element did not become visible.
     */
    @Command(name="waitForNotVisible", arguments = {"locator"}, example = "|waitForNotVisible; |//path/to/input (of css=, id=, name=)|")
    public boolean waitForNotVisible(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean visible = wait.until(ExpectedConditions.invisibilityOfElementLocated(constructLocatorFromString(locator)));
            if (!visible) {
                ConditionalExceptionType.fail("Element with locator '%s' dit not become invisible (visible but also width and height != 0) within '%d' seconds.", locator, seconds);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("Element with locator '%s' dit not become invisible (visible but also width and height != 0) within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Waits for the text to become visible anywhere on the page.
     *
     * @param text the text to search for.
     * @throws CommandException when the text did not become visible.
     */
    @Command(name="waitForTextPresent", arguments = {"text"}, example = "|waitForTextPresent; |text|")
    public boolean waitForTextPresent(String text) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean present = wait.until(textToBePresent(text));
            if (!present) {
                ConditionalExceptionType.fail("The text '%s' was not visible after '%d' seconds.", text, seconds);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("The text '%s' dit not become present within '%d' seconds, a timeout occurred.", text, seconds);
        }

        return true;
    }

    /**
     * Waits for the text to become absent anywhere on the page.
     *
     * @param text the text to search for.
     * @throws CommandException when the text did not become visible.
     */
    @Command(name="waitForTextNotPresent", arguments = {"text"}, example = "|waitForTextNotPresent; |text|")
    public boolean waitForTextNotPresent(String text) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean present = wait.until(ExpectedConditions.not(textToBePresent(text)));
            if (!present) {
                ConditionalExceptionType.fail("The text '%s' was still visible after '%d' seconds.", text, seconds);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("The text did was still present after '%d' seconds, a timeout occurred.", text, seconds);
        }

        return true;
    }

    /**
     * Waits for the text to become visible in the specified locator.
     *
     * @param locator the locator to use to find the element.
     * @param text the text to search for.
     * @throws CommandException when the text did not become visible.
     */
    @Command(name="waitForText", arguments = {"locator", "text"}, example = "|waitForText; |//path/to/select (of css=, id=, name=)|text|")
    public boolean waitForText(String locator, String text) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean present = wait.until(ExpectedConditions.textToBePresentInElement(element, text));
            if (!present) {
                ConditionalExceptionType.fail("The element with locator '%s' did not have the text '%s'.", locator, text);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("The element with locator '%s' did not have the text '%s', a timeout occurred.", locator, text);
        }

        return true;
    }

    /**
     * Waits for the specified text to be removed / adjusted.
     *
     * @param locator the locator to use to find the element.
     * @param text the text to search for.
     * @throws CommandException when the text did not change.
     */
    @Command(name="waitForNotText", arguments = {"locator", "text"}, example = "|waitForNotText; |//path/to/select (of css=, id=, name=)|text|")
    public boolean waitForNotText(String locator, String text) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean present = wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(element, text)));
            if (!present) {
                ConditionalExceptionType.fail("The element with locator '%s' still has the text '%s'.", locator, text);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("The element with locator '%s' still has the text '%s', a timeout occurred.", locator, text);
        }

        return true;
    }

    /**
     * Waits for the text to become absent anywhere on the page.
     *
     * @param locator the locator to use to find the element.
     * @param value the value to search for.
     * @throws CommandException when the text did not become visible.
     */
    @Command(name="waitForValue", arguments = {"locator", "value"}, example = "|waitForValue; |//path/to/select (of css=, id=, name=)|value|")
    public boolean waitForValue(String locator, String value) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean present = wait.until(valueToBePresentInElement(locator, value));
            if (!present) {
                ConditionalExceptionType.fail("The element with locator '%s' did not have the value '%s'.", locator, value);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("The element with locator '%s' did not have the value '%s', a timeout occurred.", locator, value);
        }

        return true;
    }

    /**
     * Waits for the element to have a value.
     *
     * @param locator the locator to use to find the element.
     * @throws CommandException when the text did not become visible.
     */
    @Command(name="waitForValueNotEmpty", arguments = {"locator"}, example = "|waitForValueNotEmpty; |//path/to/select (of css=, id=, name=)|")
    public boolean waitForValueNotEmpty(String locator) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            Boolean present = wait.until(elementNotEmpty(locator));
            if (!present) {
                ConditionalExceptionType.fail("The element at the locator '%s' did not have a value.", locator);
            }
        } catch (TimeoutException timeoutException) {
            ConditionalExceptionType.fail("The element with locator '%s' still not has a value after '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Selects an option in a select box using the specified label.
     *
     * @param locator the locator to use to find the element.
     * @param label the label of the option within the select box.
     *
     * @return true if successful
     *
     * @throws CommandException when the element could not be found, is not an SELECT element, there is no option with the specified label.
     */
    @Command(name="selectUsingLabel", arguments = {"locator", "label"}, example = "|selectUsingLabel; |//path/to/select (of css=, id=, name=)|label|")
    public boolean selectUsingLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);

        if (option.isSelected()) {
            ConditionalExceptionType.fail("The option with label '%s' cannot be selected since the option was already selected.", label);
        }

        select.selectByVisibleText(label);
        return true;
    }

    /**
     * Deselects an option in a select box using the specified label.
     *
     * @param locator the locator to use to find the element.
     * @param label the label of the option within the select box.
     *
     * @return true if successful
     *
     * @throws CommandException when the element could not be found, is not an SELECT element,
     * there is no option with the specified label or when the option was not selected in the first place.
     */
    @Command(name="deselectUsingLabel", arguments = {"locator", "label"}, example = "|deselectUsingLabel; |//path/to/select (of css=, id=, name=)|label|")
    public boolean deselectUsingLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);

        if (!option.isSelected()) {
            ConditionalExceptionType.fail("The option with label '%s' cannot be deselected since the option was not selected.", label);
        }

        select.deselectByVisibleText(label);
        return true;
    }

    /**
     * Selects an option in a select box using the specified value.
     *
     * @param locator the locator to use to find the element.
     * @param value the value of the option within the select box.
     *
     * @return true if successful
     *
     * @throws CommandException when the element could not be found, is not an SELECT element, there is no option with the specified value.
     */
    @Command(name="select", arguments = {"locator", "value"}, example = "|select; |//path/to/select (of css=, id=, name=)|value|")
    public boolean select(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);

        if (option.isSelected()) {
            ConditionalExceptionType.fail("The option with value '%s' cannot be selected since the option was already selected.", value);
        }

        select.selectByValue(value);
        return true;
    }

    /**
     * Selects an option in a select box using the specified value to trigger a page load.
     *
     * @param locator the locator to use to find the element.
     * @param value the value of the option within the select box.
     *
     * @return true if successful
     *
     * @throws CommandException when the element could not be found, is not an SELECT element, there is no option with the specified value.
     */
    @Command(name="selectAndWait (Deprecated, timeout implicit)", arguments = {"locator", "value"}, example = "|selectAndWait; |//path/to/select (of css=, id=, name=)|value|")
    public boolean selectAndWait(String locator, String value) {
        return select(locator, value);
    }

    /**
     * Deselects an option in a select box using the specified value.
     *
     * @param locator the locator to use to find the element.
     * @param value the value of the option within the select box.
     *
     * @return true if successful
     *
     * @throws CommandException when the element could not be found, is not an SELECT element,
     * there is no option with the specified value or when the option was not selected in the first place.
     */
    @Command(name="deselect", arguments = {"locator", "value"}, example = "|deselect; |//path/to/select (of css=, id=, name=)|value|")
    public boolean deselect(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);

        if (!option.isSelected()) {
            ConditionalExceptionType.fail("The option with value '%s' cannot be deselected since the option was not selected.", value);
        }

        select.deselectByValue(value);
        return true;
    }

    /**
     * Selects all options in a select.
     *
     * @param locator the locator to use to find the element.
     *
     * @return true if successful
     *
     * @throws CommandException when the element could not be found or when the element is not an SELECT element.
     */
    @Command(name="selectAllOptions", arguments = {"locator"}, example = "|selectAllOptions; |//path/to/select (of css=, id=, name=)|")
    public boolean selectAllOptions(String locator) {
        Select select = findSelect(locator);
        for (WebElement option : select.getOptions()) {
            if (!option.isSelected()) {
                select.selectByVisibleText(option.getText());
            }
        }

        return true;
    }

    /**
     * Deselects all options in a select box.
     *
     * @param locator the locator to use to find the element.
     *
     * @return true if successful
     *
     * @throws CommandException when the element could not be found.
     */
    @Command(name="deselectAllOptions", arguments = {"locator"}, example = "|deselectAllOptions; |//path/to/select (of css=, id=, name=)|")
    public boolean deselectAllOptions(String locator) {
        Select select = findSelect(locator);
        select.deselectAll();
        return true;
    }

    /**
     * Verifies if an option in a select box is selected.
     *
     * @param locator the locator to use to find the element.
     * param value the value of the option.
     */
    @Command(name="verifyOptionSelected", arguments = {"locator", "value"}, example = "|verifyOptionSelected; |//path/to/select (of css=, id=, name=)|value|")
    public boolean verifyOptionSelected(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);
        if (option.isSelected()) {
            return true;
        }

        ConditionalExceptionType.fail("The option with value '%s' is not selected, while this test suggested it should be selected.", value);
    }

    /**
     * Verifies if an option in a select box is not selected.
     *
     * @param locator the locator to use to find the element.
     * param value the value of the option.
     */
    @Command(name="verifyOptionNotSelected", arguments = {"locator", "value"}, example = "|verifyOptionNotSelected; |//path/to/select (of css=, id=, name=)|value|")
    public boolean verifyOptionNotSelected(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);
        if (!option.isSelected()) {
            return true;
        }

        ConditionalExceptionType.fail("The option with value '%s' is selected, while this test suggested it should be deselected.", value);
    }

    /**
     * Verifies if an option in a select box is selected.
     *
     * @param locator the locator to use to find the element.
     * param label the label of the option.
     */
    @Command(name="verifyOptionSelectedByLabel", arguments = {"locator", "label"}, example = "|verifyOptionSelectedByLabel; |//path/to/select (of css=, id=, name=)|label|")
    public boolean verifyOptionSelectedByLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);
        if (option.isSelected()) {
            return true;
        }

        ConditionalExceptionType.fail("The option with label '%s' is not selected, while this test suggested it should be selected.", label);
    }

    /**
     * Verifies if an option in a select box is not selected.
     *
     * @param locator the locator to use to find the element.
     * @param label the label of the option.
     * @return <code>true</code> if the option is not selected.
     */
    @Command(name="verifyOptionNotSelectedByLabel", arguments = {"locator", "label"}, example = "|verifyOptionNotSelectedByLabel; |//path/to/select (of css=, id=, name=)|label|")
    public boolean verifyOptionNotSelectedByLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);
        if (!option.isSelected()) {
            return true;
        }

        ConditionalExceptionType.fail("The option with label '%s' is selected, while this test suggested it should be deselected.", label);
    }

    /**
     * Verifies if all the options in a select box are selected.
     *
     * @param locator the locator to use to find the element.
     * @return <code>true</code> if all options are selected.
     */
    @Command(name="verifyAllOptionsSelected", arguments = {"locator"}, example = "|verifyAllOptionsSelected; |//path/to/select (of css=, id=, name=)|")
    public boolean verifyAllOptionsSelected(String locator) {
        Select select = findSelect(locator);

        ArrayList<String> labels = new ArrayList<>();
        for (WebElement option : select.getOptions()) {
            if (!option.isSelected()) {
                labels.add(option.getText());
            }
        }

        if (labels.isEmpty()) {
            return true;
        }

        ConditionalExceptionType.fail("The option(s) with label(s) '%s' are not selected, while this test suggested it should all be selected.", labels);
    }

    /**
     * Verifies if all the options in a select box are not selected.
     *
     * @param locator the locator to use to find the element.
     * @return <code>true</code> if none of the options are selected.
     */
    @Command(name="verifyAllOptionsNotSelected", arguments = {"locator"}, example = "|verifyAllOptionsNotSelected; |//path/to/select (of css=, id=, name=)|")
    public boolean verifyAllOptionsNotSelected(String locator) {
        Select select = findSelect(locator);

        ArrayList<String> labels = new ArrayList<>();
        for (WebElement option : select.getOptions()) {
            if (option.isSelected()) {
                labels.add(option.getText());
            }
        }

        if (labels.isEmpty()) {
            return true;
        }

        ConditionalExceptionType.fail("The option(s) with label(s) '%s' are selected, while this test suggested it should all be deselected.", labels);
    }
    
    /**
     * Takes a screenshot of the webpage
     */
    @Command(name = "makeScreenshotAndSave", arguments = {}, example = "|show|makeScreenshotAndSave;|")
    public String makeScreenshotAndSave() {

        File dir = new File(SCREENSHOTLOCATION);
        String fileName = "screenshot_" + new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date()) + ".png";

        Screenshot screenshot = new AShot().takeScreenshot(driver);
        try {
            ImageIO.write(screenshot.getImage(), "PNG", new File(dir, fileName));
        } catch (IOException e) {
            // Do not let the test fail because of this.
            e.printStackTrace();
        }

        return "<a href=\"files/screenshots/" + fileName + "\"><img src=./files/screenshots/" + fileName + " width=\"300\"></a>";
    }
    
    /**
     * Takes a screenshot of a specified element
     */

    @Command(name = "makeScreenshotAndSave", arguments = {"locator"}, example = "|show|makeScreenshotAndSave;|//path/to/select (//path/to/select (of css=, id=, name=)|")
    public String makeScreenShotAndSave(String locator) {

        File dir = new File(SCREENSHOTLOCATION);
        String fileName = "screenshot_partial_" + new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date()) + ".png";

        Screenshot screenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).imageCropper(new IndentCropper().addIndentFilter(new BlurFilter())).takeScreenshot(driver, findElement(locator));
        try {
            ImageIO.write(screenshot.getImage(), "PNG", new File(dir, fileName));
        } catch (IOException e) {
            // Do not let the test fail because of this.
            e.printStackTrace();
        }

        return "<a href=\"files/screenshots/" + fileName + "\" target=\"_blank\"><img src=./files/screenshots/" + fileName + " width=\"300\"></a>";
    }

    /**
     * Finds an element using the locator that has the <code>select</code> tag.
     * @param locator the locator to find the element
     * @return the Select element.
     * @throws CommandException when an element was found using the locator but was not a <code>select</code> element.
     */
    private Select findSelect(String locator) {
        WebElement element = findElement(locator);

        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase("select")) {
            return new Select(element);
        } else {
            ConditionalExceptionType.fail("Element found using the locator '%s' is not a select element but a '%s' element.", locator, tagName);
        }
    }

    /**
     * Finds an option inside a <code>select</code> element.
     * @param select the select element.
     * @param label the label to search for.
     * @return the option element.
     * @throws CommandException when the <code>select</code> element does not contain an option with the specified label.
     */
    private WebElement findOptionInSelectByLabel(Select select, String label) {
        for (WebElement option : select.getOptions()) {
            if (option.getText().equals(label)) {
                return option;
            }
        }

        ConditionalExceptionType.fail("The select element does not have an option with label '%s'", label);
    }

    /**
     * Finds an option inside a <code>select</code> element.
     * @param select the select element.
     * @param value the label to search for.
     * @return the option element.
     * @throws CommandException when the <code>select</code> element does not contain an option with the specified value.
     */
    private WebElement findOptionInSelectByValue(Select select, String value) {
        for (WebElement option : select.getOptions()) {
            if (option.getAttribute(ATTR_VALUE).equals(value)) {
                return option;
            }
        }

        ConditionalExceptionType.fail("The select element does not have an option with value '%s'", value);
    }

    /**
     * Finds an element on the page.
     * @param locator the locator to use to find the element.
     * @return the element found using the locator.
     * @throws CommandException when the element could not be found.
     */
    private WebElement findElement(String locator) {
        try {
            return driver.findElement(constructLocatorFromString(locator));
        } catch (NoSuchElementException e) {
            ConditionalExceptionType.fail("No element found using the locator '%s'.", locator);
        }
    }

    /**
     * Constructs a selector based on the locator prefix.
     *
     * @param locator the locator.
     *
     * @return the selector.
     */
    private static By constructLocatorFromString(String locator) {
        if (locator.startsWith(LOCATOR_KPATH)) {
            String cssSelector = "*[so-key=\"" + locator.substring(LOCATOR_KPATH.length()).replace("/", "\"] *[so-key=\"") + "\"]";
            return By.cssSelector(cssSelector);
        } else if (locator.startsWith(LOCATOR_XPATH)) {
            return By.xpath(locator.substring(LOCATOR_XPATH.length()));
        } else if (locator.startsWith(LOCATOR_CSS)) {
            return By.cssSelector(locator.substring(LOCATOR_CSS.length()));
        } else if (locator.startsWith(LOCATOR_ID)) {
            return By.id(locator.substring(LOCATOR_ID.length()));
        } else if (locator.startsWith(LOCATOR_NAME)) {
            return By.name(locator.substring(LOCATOR_NAME.length()));
        } else if (locator.startsWith(LOCATOR_CLASSNAME)) {
            return By.className(locator.substring(LOCATOR_CLASSNAME.length()));
        } else if (locator.startsWith(LOCATOR_LINKTEXT)) {
            return By.linkText(locator.substring(LOCATOR_LINKTEXT.length()));
        } else if (locator.startsWith(LOCATOR_PARTIAlLINKTEXT)) {
            return By.partialLinkText(locator.substring(LOCATOR_PARTIAlLINKTEXT.length()));
        } else {
            ConditionalExceptionType.fail("The locator '%s' contains an invalid prefix, please use any of the following '%s', '%s', '%s', '%s', '%s'.", locator, LOCATOR_KPATH, LOCATOR_CSS, LOCATOR_XPATH, LOCATOR_ID, LOCATOR_NAME);
        }
    }

    /**
     * Get the actual value of an element, for checkboxes and radio buttons it will return <code>true</code> or <code>false</code>.
     * @param element the element
     * @return the value or null.
     */
    private static String getValueElement(WebElement element) {
        String value = null;

        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT)) {
            String type = element.getAttribute(ATTR_TYPE);
            if (type.equalsIgnoreCase(TYPE_CHECKBOX) || type.equalsIgnoreCase(TYPE_RADIO)) {
                value = element.isSelected() ? Boolean.TRUE.toString() : Boolean.FALSE.toString();
            } else {
                value = element.getAttribute(ATTR_VALUE);
            }
        } else if (tagName.equalsIgnoreCase(ELEMENT_TEXTAREA)) {
            value = element.getAttribute(ATTR_VALUE);
        }

        return value;
    }
    
    /**
     * Method to send inut to a INPUT element with TYPE="FILE". If a RemoteWebdriver is used the file will be streamed to the selenium node. If a local Webdriver is used this command will type the filename in the input field.
     *
     * @param locator
     * @param fileName
     */

    @Command(name = "uploadFile", arguments = {"locator", "filename"}, example = "|uploadFile;|//path/to/input (of css=, id=, name=, classname=)|filename|")
    public void uploadFile(String locator, String fileName) {
        final String nameOfFile = fileName;
        final String locatorOfElement = locator;
        FileFinder.findFileInUserDirectory("selenium.properties", new FileHandler() {
            @Override
            public void handle(File file) {
                try {
                    Properties properties = new Properties();
                    properties.load(new FileInputStream(file));

                    String browser = (String) properties.get("webdriver.browser");

                    if (browser.equalsIgnoreCase(BROWSER_REMOTE)) {
                        WebElement element = findElement(locatorOfElement);
                        ((RemoteWebElement) element).setFileDetector(new LocalFileDetector());
                        element.sendKeys(nameOfFile);
                    } else {
                        type(locatorOfElement, nameOfFile);
                    }
                } catch (FileNotFoundException fnfe) {
                    ConditionalExceptionType.fail("The file 'selenium.properties' cannot be found.");
                } catch (IOException ie) {
                    ConditionalExceptionType.fail("Error while reading file 'selenium.properties'.");
                } catch (NullPointerException np) {
                    ConditionalExceptionType.fail("The spcified file was not found");
                }
            }
        });
    }

    /**
     * Returns a reference to the mouse based on the driver instantiated.
     * @return reference to the mouse.
     */
    private Mouse getMouse() {
        if (driver instanceof FirefoxDriver) {
            return ((FirefoxDriver)driver).getMouse();
        } else if (driver instanceof ChromeDriver) {
            return ((ChromeDriver)driver).getMouse();
        } else if (driver instanceof InternetExplorerDriver) {
            return ((InternetExplorerDriver)driver).getMouse();
        } else if (driver instanceof SafariDriver) {
            return ((SafariDriver) driver).getMouse();
        } else if (driver instanceof RemoteWebDriver) {
            return ((RemoteWebDriver) driver).getMouse();
        } else {
            return ((HtmlUnitDriver)driver).getMouse();
        }
    }

    /**
     * Initialize the correct WebDriver for the browser specified.
     */
    private void initDriver(String configuration) {
        FileFinder.findFileInUserPath(configuration, file -> {
            try {
                Properties properties = new Properties();
                properties.load(new FileInputStream(file));

                String browser = (String) properties.get("webdriver.browser");
                screenshotLocation = (String)properties.get("screenshot.location");
                if (browser.equalsIgnoreCase(BROWSER_FIREFOX)) {
                    initFirefoxDriver(properties);
                } else if (browser.equalsIgnoreCase(BROWSER_CHROME)) {
                    driver = new ChromeDriver();
                } else if (browser.equalsIgnoreCase(BROWSER_SAFARI)) {
                    driver = new SafariDriver();
                } else if (browser.equalsIgnoreCase(BROWSER_INTERNET_EXPLORER)) {
                    driver = new InternetExplorerDriver();
                } else if (browser.equalsIgnoreCase(BROWSER_REMOTE)) {
                    initRemoteDriver(properties);
                } else if (browser.equalsIgnoreCase(BROWSER_HEADLESS)) {
                    initHeadlessDriver();
                } else {
                    throw new CommandException("The browser '%s' is not supported, please use any of the following '%s','%s', '%s' or '%s'.", browser, BROWSER_FIREFOX, BROWSER_CHROME, BROWSER_SAFARI, BROWSER_HEADLESS);
                }
            } catch (FileNotFoundException fnfe) {
                ConditionalExceptionType.fail("The file 'selenium.properties' cannot be found.");
            } catch (IOException ie) {
                ConditionalExceptionType.fail("Error while reading file 'selenium.properties'.");
            }
        });
    }

    private void initFirefoxDriver(Properties properties) {
        if (properties.containsKey("webdriver.firefox.profile")) {
            FileFinder.findFolderInUserPath((String) properties.get("webdriver.firefox.profile"), firefoxProfileDir -> {
                FirefoxProfile profile = new FirefoxProfile(firefoxProfileDir);
                addProxyWhenNeededThroughProfile(profile, properties);
                SlimWebDriver.this.driver = new FirefoxDriver(profile);
            });
        } else {
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            addProxyWhenNeededThroughDesiredCapabilities(desiredCapabilities, properties);
            driver = new FirefoxDriver(desiredCapabilities);
        }
    }

    /**
     * Adds when configured the proxy settings to the desired capabilities for Firefox.
     *
     * @param desiredCapabilities the desired capabilities.
     * @param properties the properties configuration.
     */
    private void addProxyWhenNeededThroughDesiredCapabilities(DesiredCapabilities desiredCapabilities, Properties properties) {
        if (properties.containsKey("webdriver.firefox.proxy.enable") &&
                Boolean.valueOf(properties.getProperty("webdriver.firefox.proxy.enable"))) {
            String proxyHost = properties.getProperty("webdriver.firefox.proxy.host");
            String proxyPort = properties.getProperty("webdriver.firefox.proxy.port");
            String proxyUrl = proxyHost + ":" + proxyPort;

            Proxy proxy = new Proxy()
                    .setFtpProxy(proxyUrl)
                    .setHttpProxy(proxyUrl)
                    .setSslProxy(proxyUrl)
                    .setSocksProxy(proxyUrl);

            desiredCapabilities.setCapability(CapabilityType.PROXY, proxy);
        }
    }

    /**
     * Adds when configured the proxy settings to the firefox profile.
     *
     * @param profile the Firefox profile.
     * @param properties the properties configuration.
     */
    private void addProxyWhenNeededThroughProfile(FirefoxProfile profile, Properties properties) {
        if (properties.containsKey("webdriver.firefox.proxy.enable") &&
                Boolean.valueOf(properties.getProperty("webdriver.firefox.proxy.enable"))) {
            String proxyHost = properties.getProperty("webdriver.firefox.proxy.host");
            String proxyPort = properties.getProperty("webdriver.firefox.proxy.port");

            profile.setPreference("network.proxy.ftp", proxyHost);
            profile.setPreference("network.proxy.ftp_port", proxyPort);
            profile.setPreference("network.proxy.http", proxyHost);
            profile.setPreference("network.proxy.http_port", proxyPort);
            profile.setPreference("network.proxy.socks", proxyHost);
            profile.setPreference("network.proxy.socks_port", proxyPort);
            profile.setPreference("network.proxy.ssl", proxyHost);
            profile.setPreference("network.proxy.ssl_port", proxyPort);
        }
    }


    private void initRemoteDriver(Properties properties) throws MalformedURLException {
        String remoteBrowser = (String) properties.get("webdriver.remote.browser");
        String remoteBrowserVersion = (String) properties.get("webdriver.remote.browser.version");
        
        String url = (String) properties.get("webdriver.remote.server.url");
        url = url + (url.endsWith("/") ? "" : "/") + "wd/hub";
        URL remoteServerUrl = new URL(url);
        
        DesiredCapabilities capabilities = new DesiredCapabilities(remoteBrowser, remoteBrowserVersion, Platform.ANY);
        
        if (properties.containsKey("webdriver.remote.firefox.profile")) {
            FileFinder.findDirectoryInUserDirectory((String) properties.get("webdriver.remote.firefox.profile"), new FileHandler() {
                @Override
                public void handle(File firefoxProfileDir) {
                    remoteProfile = new FirefoxProfile(firefoxProfileDir);
                }
            });
            capabilities.setCapability(FirefoxDriver.PROFILE, remoteProfile);
        }

        driver = new RemoteWebDriver(remoteServerUrl, capabilities);
    }

    private void initHeadlessDriver() {
        HtmlUnitDriver webdriver = new HtmlUnitDriver(BrowserVersion.FIREFOX_38);
        webdriver.setJavascriptEnabled(true);
        driver = webdriver;
    }

    /**
     * Condition to check if the text is present anywhere on the page.
     * @param text the text to search for.
     * @return flag indicating if it did or did not become visible.
     */
    public static ExpectedCondition<Boolean> textToBePresent(final String text) {

        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return driver.getPageSource().contains(text);
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("text ('%s') to be present anywhere on the page", text);
            }
        };
    }

    /**
     * Condition to check if the text is present anywhere on the page.
     * @param locator the locator to use to find the element.
     * @return flag indicating if it did or did not become visible.
     */
    public static ExpectedCondition<Boolean> elementNotEmpty(final String locator) {

        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    WebElement element = driver.findElement(constructLocatorFromString(locator));
                    String valueElement = getValueElement(element);
                    return valueElement != null && !valueElement.isEmpty();
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("locator '%s' to be present on the page and have a value.", locator);
            }
        };
    }

    /**
     * Condition to check if the text is present anywhere on the page.
     * @param locator the locator to use to find the element.
     * @param value the value to look for.
     * @return flag indicating if it did or did not become visible.
     */
    public static ExpectedCondition<Boolean> valueToBePresentInElement(final String locator, final String value) {

        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    WebElement element = driver.findElement(constructLocatorFromString(locator));
                    String valueElement = getValueElement(element);
                    return valueElement != null && valueElement.equalsIgnoreCase(value);
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("locator '%s' to be present on the page and have a value.", locator);
            }
        };
    }
    
    @Command(name = "setLoopCommand", arguments = {"command"}, example = "|setLoopCommand;|command|")
    public void setLoopCommand(String command) {
        loopCommands.add(new WebdriverCommand(command));
    }

    @Command(name = "setLoopCommand", arguments = {"command", "target"}, example = "|setLoopCommand;|command|target|")
    public void setLoopCommand(String command, String target) {
        loopCommands.add(new WebdriverCommand(command, target));
    }

    @Command(name = "setLoopCommand", arguments = {"command", "target", "value"}, example = "|setLoopCommand;|command|target|value")
    public void setLoopCommand(String command, String target, String value) {
        loopCommands.add(new WebdriverCommand(command, target, value));
    }

    @Command(name = "resetLoopCommands", arguments = {}, example = "|resetLoopCommands;|")
    public void resetLoopCommands() {
        loopCommands.clear();
    }

    @Command(name = "runLoop", arguments = {"numberOfTimes", "timeBetweenLoopsInMillis"}, example = "|runLoop;|numberOfTimes|timeBetweenLoopsInMillis|")
    public void runLoop(int numberOfTimes, int timeBetweenLoopsInMillis) {
        for (int x = 0; x < numberOfTimes; x++) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
            try {
                Thread.sleep(timeBetweenLoopsInMillis);
            } catch (InterruptedException e) {
                //do nothing
            }
        }
    }

    @Command(name = "runLoopCommandIfElementIsPresent", arguments = {"locator"}, example = "|runLoopCommandIfElementIsPresent;|//path/to/select (of css=, id=, name=, classname=, link=, partiallink=)|")
    public void runLoopCommandIfElementIsPresent(String locator) {
        if (verifyElementPresent(locator)) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
        }
    }

    @Command(name = "runLoopUntilElementPresent", arguments = {"locator", "maxTimesToLoop", "timeBetweenLoopsInMillis"}, example = "|runLoopUntilElementPresent;|//path/to/select (of css=, id=, name=, classname=, link=, partiallink=)|maxTimesToLoop|timeBetweenLoopsInMillis")
    public boolean runLoopUntilElementPresent(String locator, int maxTimesToLoop, int timeBetweenLoopsInMillis) {
        int current = 0;
        while (current < maxTimesToLoop && !verifyElementPresent(locator)) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
            try {
                Thread.sleep(timeBetweenLoopsInMillis);
            } catch (InterruptedException e) {
                //do nothing
            }
            current++;
        }
        return verifyElementPresent(locator);
    }

    @Command(name = "runLoopUntilJavascriptReturnValue", arguments = {"javascript", "expectedExpressionReturnValue", "maxTimesToLoop", "timeBetweenLoopsInMillis"}, example = "|runLoopUntilJavascriptReturnValue;|javascript|expectedExpressionReturnValue|maxTimesToLoop|timeBetweenLoopsInMillis")
    public boolean runLoopUntilJavascriptReturnValue(String javascript, String expectedExpressionReturnValue, int maxTimesToLoop, int timeBetweenLoopsInMillis) {
        int current = 0;
        while (current < maxTimesToLoop && !((JavascriptExecutor) driver).executeScript(javascript).equals(expectedExpressionReturnValue)) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
            try {
                Thread.sleep(timeBetweenLoopsInMillis);
            } catch (InterruptedException e) {
                //do nothing
            }
            current++;
        }
        return ((JavascriptExecutor) driver).executeScript(javascript).equals(expectedExpressionReturnValue);
    }
}