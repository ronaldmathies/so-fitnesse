package nl.kvk.fitnesse;

import fitnesse.util.FitnesseSystemSettings;

/**
 * @author Ronald Mathies
 */
public class ConditionalExceptionType {

    public static void fail(String message, Object ...args) throws CommandException  {
        if (FitnesseSystemSettings.stopTestOnException()) {
            throw new StopTestCommandException(message, args);
        }

        throw new CommandException(message, args);
    }
}
