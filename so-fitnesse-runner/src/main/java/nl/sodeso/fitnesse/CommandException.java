package nl.sodeso.fitnesse;

/**
 * @author Ronald Mathies
 */
public class CommandException extends RuntimeException {

    public CommandException(String message, Object... args) {
        super("message:<<" + String.format(message, args) + ">>");
    }
}
