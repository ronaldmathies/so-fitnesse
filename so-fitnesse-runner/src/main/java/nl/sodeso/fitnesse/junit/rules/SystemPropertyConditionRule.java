package nl.sodeso.fitnesse.junit.rules;

import nl.sodeso.fitnesse.junit.ConditionRule;

/**
 * @author Ronald MAthies
 */
public class SystemPropertyConditionRule implements ConditionRule {

    private static final String SYSTEM_FITNESSE_SKIP_TEST_KEY = "fitnesseSkipTest";

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean execute() {
        String property = System.getProperty(SYSTEM_FITNESSE_SKIP_TEST_KEY);
        if (property == null) {
            return false;
        }

        return Boolean.valueOf(property);
    }
}
