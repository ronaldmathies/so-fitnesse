package nl.sodeso.fitnesse.junit;

import fitnesse.junit.FitNesseRunner;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

/**
 * @author Ronald Mathies
 */
public class ConditionalFitNesseRunner extends Runner {

    private FitNesseRunner runner = null;
    private Class<?> suiteClass = null;
    private boolean isExecutionAllowed = false;

    /**
     * {@inheritDoc}
     */
    public ConditionalFitNesseRunner(Class<?> suiteClass) throws InitializationError {
        this.suiteClass = suiteClass;

        isExecutionAllowed = isExecutionAllowed();
        if (isExecutionAllowed) {
            try {
                runner = new FitNesseRunner(suiteClass);
            } catch (InitializationError e) {
                throw new ConditionalFitnesseRunnerException(e);
            }
        }
    }

    /**
     * Executes all the conditions, if a condition results in not allowing the test to run it will
     * immediately stop processing the remaining conditions and return false.
     *
     * @return true if execution is allowed, false if not.
     */
    private boolean isExecutionAllowed() {
        try {
            Condition[] conditions = suiteClass.getAnnotationsByType(Condition.class);
            if (conditions != null) {
                for (Condition condition : conditions) {

                    ConditionRule conditionRule = condition.rule().newInstance();
                    if (!conditionRule.execute()) {
                        return false;
                    }

                }
            }
        } catch (IllegalAccessException | InstantiationException e) {
            throw new ConditionalFitnesseRunnerException(e);
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Description getDescription() {
        if (this.isExecutionAllowed) {
            return runner.getDescription();
        }

        return Description.createSuiteDescription(ConditionalFitNesseRunner.class.getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run(RunNotifier runNotifier) {
        if (isExecutionAllowed) {
            runner.run(runNotifier);
        } else {
            runNotifier.pleaseStop();
        }
    }
}
