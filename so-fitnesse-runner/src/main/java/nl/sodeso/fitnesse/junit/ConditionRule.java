package nl.sodeso.fitnesse.junit;

/**
 * Interface used for determining if a fitnesse unit-test should run or not.
 *
 * @author Ronald Mathies
 */
public interface ConditionRule {

    /**
     * Flag indicating if the test should run (true) or not (false).
     * @return true to run the test, false to skip the test.
     */
    boolean execute();

}
