package nl.sodeso.fitnesse.junit;

/**
 * @author Ronald Mathies
 */
public class ConditionalFitnesseRunnerException extends RuntimeException {

    public ConditionalFitnesseRunnerException(Throwable throwable) {
        super(throwable);
    }
}
