package nl.sodeso.fitnesse.junit;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Ronald Mathies
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Condition {

    /**
     * The rule to which will be used to determin if the fitnesse unit-test should run or not.
     * @return the rule implementing the <code>ConditionRule</code> interface.
     */
    Class<? extends ConditionRule> rule();

}
