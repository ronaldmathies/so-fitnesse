package nl.sodeso.fitnesse.junit;

import fitnesse.junit.FitNesseRunner;
import nl.sodeso.fitnesse.junit.rules.SystemPropertyConditionRule;
import org.junit.runner.RunWith;

import fitnesse.junit.FitNesseRunner.FitnesseDir;
import fitnesse.junit.FitNesseRunner.OutputDir;
import fitnesse.junit.FitNesseRunner.ConfigFile;

@Condition(rule = SystemPropertyConditionRule.class)
@RunWith(ConditionalFitNesseRunner.class)
@FitNesseRunner.Suite("TestSuite")
@FitnesseDir("src/test/resources")
@OutputDir("target/fitnesse-report")
@ConfigFile("src/test/resources/plugins.properties")
public class FitNesseSuiteExampleTest {

}