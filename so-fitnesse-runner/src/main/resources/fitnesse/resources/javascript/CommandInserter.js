function CommandInserter()
{
  this.insertInto = function(commandValue, codeMirrorDoc) {

    if(commandValue !== "")
    {
      var pageDataUrl = commandValue + "?commandData";

      $.ajax({
        url: pageDataUrl,
        success: function(result) {
          codeMirrorDoc.replaceSelection(result);
        },
        error: function() {
          alert("Error Accessing Template");
        }
      });
    }
  };
}
