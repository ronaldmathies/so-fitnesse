package nl.sodeso.fitnesse.fixtures.database;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import fitnesse.slim.SystemUnderTest;
import nl.sodeso.fitnesse.CommandException;
import nl.sodeso.fitnesse.fixtures.general.properties.PropertiesFixture;
import nl.sodeso.persistence.jdbc.SqlScriptExecutor;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Database Fixture")
public class DatabaseFixture {

    private static final String CONNECTION_DATABASE_DRIVER = "database.driver";
    private static final String CONNECTION_DATABASE_URL = "database.url";
    private static final String CONNECTION_DATABASE_USERNAME = "database.username";
    private static final String CONNECTION_DATABASE_PASSWORD = "database.password";

    private Map<String, Connection> connections = new HashMap<String, Connection>();

    private Map<String, ResultSetManager> resultSetManagers = new HashMap<>();

    @SystemUnderTest
    protected ResultSetManager activeResultSetManager = null;

    /**
     * Constructs a new database fixture.
     */
    @Start(name = "Start Database Fixture", arguments = {}, example = "|start |Database Fixture|")
    public DatabaseFixture() {}

    /**
     * Creates a new database connection with the specified name and properties.
     *
     * @param name the name for the connection.
     * @param properties the properties file containing the connection settings.
     */
    @Command(name = "Create Connection", arguments = {"name", "properties"}, example = "|createConnection; |name|database.properties |")
    public void createConnection(String name, String properties) {
        Connection connection = createConnection(properties);
        connections.put(name, connection);
    }

    /**
     * Performs a query on the database using the connection with the specified name.
     *
     * @param name the name of the connection to use.
     * @param sql the sql statement to execute.
     *
     * @return the resultset.
     */
    @Command(name = "Perform Select", arguments = {"name", "sql"}, example = "|performSelect; |name|select count(*) from table|")
    public void performSelect(String name, String sql) {
        Connection connection = findConnectionByName(name);

        try {
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet resultSet = statement.executeQuery(sql);

            if (!resultSetManagers.containsKey(name)) {
                resultSetManagers.put(name, new ResultSetManager());
            }

            ResultSetManager resultSetManager = resultSetManagers.get(name);
            resultSetManager.setResultSet(resultSet);

            this.activeResultSetManager = resultSetManager;
        } catch (SQLException e) {
            throw new CommandException("Failed to execute query '%s' due to the following '%s'.", sql, e.getMessage());
        }
    }

    /**
     * Activates a result set, that was initiated by a previous query executed.
     *
     * @param name the name of the connection that initiated a result set.
     */
    @Command(name = "Activate Result Set", arguments = {"name"}, example = "|activateResultSet; |name|")
    public void activateResultSet(String name) {
        if (!resultSetManagers.containsKey(name)) {
            throw new CommandException("No active result set found for connection '%s'.", name);
        }

        this.activeResultSetManager = resultSetManagers.get(name);
    }

    /**
     * Retreives a value (the specified column) from the resultset at the given row..
     *
     * NOTE: This method is a facade for the SystemUnderTest object.
     */
    @Command(name = "Get Value", arguments = {"row", "column"}, example = "|$value= |getValue; |10|mycolumn|")
    private void dummyGetValue() {}

    /**
     * Retreives a value (the specified column) from the resultset at the given row..
     *
     * NOTE: This method is a facade for the SystemUnderTest object.
     */
    @Command(name = "Get Number Of Rows", arguments = {}, example = "|$count= |getNumberOfRows; |")
    private void dummyGetNumberOfRowser() {}

    /**
     * Performs a query on the database using the connection with the specified name.
     *
     * The query should result in a single row with a single column, in case of more
     * columns then only the first column will be returned.
     *
     * @param name the name of the connection to use.
     * @param sql the sql statement to execute.
     *
     * @return the resultset.
     */
    @Command(name = "Perform Select Single Result", arguments = {"name", "sql"}, example = "|performSelectSingleResult; |name|select column from table where 1 = 1|")
    public Object performSelectSingleResult(String name, String sql) {
        Connection connection = findConnectionByName(name);

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getObject(1);
            }


            return null;
        } catch (SQLException e) {
            throw new CommandException("Failed to execute query '%s' due to the following '%s'.", sql, e.getMessage());
        }
    }

    /**
     * Executes the contents of an SQL file using the connection with the specified name.
     *
     * @param name the name of the connection to use.
     * @param sqlFile the file containing the sql statements to execute.
     *
     * @return flag indicating if the execution was successful.
     */
    @Command(name = "Execute SQL File", arguments = {"name", "sqlFile"}, example = "|executeSqlFile; |name|create_table.sql|")
    public boolean executeSqlFile(String name, String sqlFile) {
        Connection connection = findConnectionByName(name);

        SqlScriptExecutor sqlExecutor = new SqlScriptExecutor();

        try {
            sqlExecutor.execute(connection, sqlFile);
        } catch (IOException e) {
            throw new CommandException("Error while executing SQL file: " + e.getMessage());
        }
        
        return true;
    }

    /**
     * Closes a database connection with the specified name.
     *
     * @param name the name of the database connection.
     */
    @Command(name = "Close Connection", arguments = {"name"}, example = "|closeConnection; |name|")
    public void closeConnection(String name) {
        Connection connection = findConnectionByName(name);
        try {
            if (resultSetManagers.containsKey(name)) {
                ResultSetManager resultSetManager = resultSetManagers.get(name);
                ResultSet resultSet = resultSetManager.getResultSet();

                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }

                resultSetManagers.remove(name);

                if (this.activeResultSetManager == resultSetManager) {
                    this.activeResultSetManager = null;
                }
            }

            connection.close();
        } catch (SQLException sqlException) {
            // Ignore this message.
        } finally {
            connections.remove(name);
        }
    }

    /**
     * Close all open database connections.
     */
    @Command(name = "Close All Connection", arguments = {}, example = "|closeAllConnections; |")
    public void closeAllConnections() {
        for (String name : connections.keySet()) {
            closeConnection(name);
        }
    }

    /**
     * Finds a database connection with the specified name.
     *
     * @param name the name of the database connection.
     *
     * @return the database connection.
     *
     * @throws CommandException when the database connection with the specified name could not be found.
     */
    private Connection findConnectionByName(String name) {
        if (connections.containsKey(name)) {
            return connections.get(name);
        }

        throw new CommandException("No database connection found with name '%s'.", name);

    }

    /**
     * Creates a new database connection with the specified properties file.
     *
     * @param properties the properties file containing the connection settings.
     *
     * @return the database connection.
     *
     * @throws CommandException when the driver class could not be found or when the connection could not be opened.
     */
    private Connection createConnection(String properties) {
        PropertiesFixture propertiesFixture = new PropertiesFixture(properties);

        String databaseDriver = propertiesFixture.getStringProperty(CONNECTION_DATABASE_DRIVER);
        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new CommandException("Cannot find database driver class '%s'.", databaseDriver);
        }

        String databaseUrl = propertiesFixture.getStringProperty(CONNECTION_DATABASE_URL);
        String databaseUsername = propertiesFixture.getStringProperty(CONNECTION_DATABASE_USERNAME);
        String databasePassword = propertiesFixture.getStringProperty(CONNECTION_DATABASE_PASSWORD);

        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException sqlException) {
            throw new CommandException("Cannot open connection to '%s' because '%s'.", databaseUrl, sqlException.getMessage());
        }
    }


}
