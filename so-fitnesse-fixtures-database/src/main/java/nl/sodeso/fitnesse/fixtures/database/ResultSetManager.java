package nl.sodeso.fitnesse.fixtures.database;

import nl.sodeso.fitnesse.CommandException;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Ronald Mathies
 */
public class ResultSetManager {

    private ResultSet resultSet;

    public ResultSetManager() {}

    public void setResultSet(ResultSet resultSet) {
        try {
            if (this.resultSet != null && !this.resultSet.isClosed()) {
                this.resultSet.close();
            }

            this.resultSet = resultSet;
        } catch (SQLException sqlException) {
            throw new CommandException("Could not close resultset due to exception '%s'.", sqlException.getMessage());
        }
    }

    public ResultSet getResultSet() {
        return this.resultSet;
    }

    public int getNumberOfRows() {
        isOpen();

        try {
            this.resultSet.last();
            return this.resultSet.getRow();
        } catch (SQLException sqlException) {
            throw new CommandException("There was a problem counting the number of rows in the resultset '%s'.", sqlException.getMessage());

        }
    }

    public Object getValue(int index, String column) {
        isOpen();

        if (index == 0) {
            throw new CommandException("Cannot get value from row 0, the row count starts with 1.");
        }

        try {
            this.resultSet.absolute(index);
            return this.resultSet.getObject(column);
        } catch (SQLException sqlException) {
            throw new CommandException("There was a problem getting a value out of the resultset '%s'.", sqlException.getMessage());
        }
    }

    private void isOpen() {
        try {
            if (resultSet.isClosed()) {
                throw new CommandException("Resultset is closed, cannot retrieve any information.");
            }
        } catch (SQLException sqlException) {
            throw new CommandException("There was a problem checking if the resultset is still open '%s'.", sqlException.getMessage());
        }
    }

}
